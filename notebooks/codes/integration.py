import os
import re
import nltk
import string
import gensim
import datetime
import calendar
import itertools
import pycountry
import numpy as np
import collections
import pandas as pd
import seaborn as sns
from nltk import ngrams
from collections import Counter
import matplotlib.pyplot as plt
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
from nltk import sent_tokenize
from gensim import corpora, models, similarities
from gensim.corpora.dictionary import Dictionary
import multiprocessing
from multiprocessing.pool import Pool
import pickle
import warnings
import configparser
warnings.filterwarnings("ignore", category=DeprecationWarning) 
pd.options.mode.chained_assignment = None  # default='warn'
pd.options.mode.chained_assignment = None  # default='warn'

from utility_functions import logdebug
from preprocessing import Preprocessing
feature_preprocessor = Preprocessing()
config = configparser.ConfigParser()
config.read('./config.ini')


class DataSetLoader():
    
    def __init__(self, verbose = 1, feature_preprocessor = None, technology = None):
        #preprocesson data sets
        self.feature_preprocessor = feature_preprocessor  
        self.verbose = verbose
        self.technology = technology
        file_paths = 'Security_File_Paths'
        self.raw_file_path = config[file_paths]['raw_file']
        self.transformed_summary = config[file_paths]['Transformed_Summary']        
        self.transformed_pd = config[file_paths]['Transformed_PD']
        self.transformed_cs = config[file_paths]['Transformed_CS']
#         self.transformed_rs = config[file_paths]['Transformed_RS']
        
#     @staticmethod  
    def extract_build_transform(self, data_path = None, summary = True,  problem_desc = True, 
                                customer_sym = True, combine = True):
        
        #if customer data path is not passed, assign it to default path
        if data_path is None:
            data_path = self.raw_file_path
            
            
        sr_collection = self.extract(data_path)
        print ('extract done')
        sr_collection = self.build(sr_collection)
        print ('build done')
        sr_collection = self.transform(sr_collection, summary, problem_desc, customer_sym,combine)
        print ('transform done')
        logdebug('All Transactions Complete', self.verbose)
        return sr_collection
    
#     @staticmethod
    def extract(self, data_path, verbose = 1):
        logdebug('Extracting Data from Location: ' + self.raw_file_path, self.verbose)
        if os.path.isfile(data_path):
            st = os.stat(data_path)    
            mtime = st.st_mtime            
            logdebug('Using cached dataframe file from {}'.format(datetime.datetime.fromtimestamp(mtime)), self.verbose)
            sr_collection = pd.read_pickle(data_path)
            logdebug('Case Data Read with Shape: ' +str(sr_collection.shape), self.verbose)
        else:
            sr_collection = None
            logdebug('Data Frame Not Available', self.verbose) 
        return sr_collection
    
#     @staticmethod
    def build(self, data):
        #Build text corpus from the Original Data Frame
        logdebug('Building Data using required Columns: ', self.verbose)
        lower_case_cols = [col.lower() for col in data.columns]
        data.columns = lower_case_cols
        required_cols = ['incident_number','summary','note_type_mod', 'notes', 'notes_detail','creation_date']
        logdebug(required_cols, self.verbose)
        data = data[required_cols]
        data = data.head(10000)
        print ("data to be processed ," , data.shape)
        return data
    
    
#     @staticmethod
    def transform(self, data, summary, customer_sym, problem_desc, combine):
        if summary:
            
            logdebug('\n\n\nSummary Transformation in Progress', self.verbose)
            data_summary = data[['incident_number', 'summary']].drop_duplicates(subset = ['incident_number'])
            data_summary = data_summary[data_summary['summary'].notnull()]
            data_summary['original_text'] = data_summary["summary"]
            
            # cleaning
            data_summ_cleaned = self.__data_clean_pp(data_summary,'original_text',self.feature_preprocessor.clean_summary)
            del data_summary
            print ('parallel proc done')
            
            data_summ_cleaned.columns = ['clean_text','incident_number','original_text']
            data_summ_cleaned['original_text_SPLITSRBYTHIS'] = data_summ_cleaned["incident_number"] + "SPLITSRBYTHIS" +  data_summ_cleaned["original_text"]
            id = list(range(0, len(data_summ_cleaned.index)))
            data_summ_cleaned['id'] = pd.Series(id).values
            data_summ_cleaned = data_summ_cleaned[['id', 'incident_number', 'original_text','original_text_SPLITSRBYTHIS', 'clean_text']]
            
            # feature extraction
            data_summ_cleaned['bugs'] = data_summ_cleaned['original_text'].apply(lambda x: self.feature_preprocessor.scrape_bugs(x))
            data_summ_cleaned['digital_signatures'] = data_summ_cleaned['original_text'].apply(lambda x: self.feature_preprocessor.scrape_signatures(x))
            
            # lambda function on two columns
#             data_summ_cleaned['different_sr'] = data_summ_cleaned['original_text_SPLITSRBYTHIS'].apply(self.feature_preprocessor.scrape_sr_no)
            data_summ_cleaned['pid'] = data_summ_cleaned['original_text'].apply(self.feature_preprocessor.extract_pid)
            data_summ_cleaned['note_type'] = 'SUMMARY'
            data_summ_cleaned.to_pickle(self.transformed_summary)
            del data_summ_cleaned
     
        if customer_sym:
        
            logdebug('\n\n\nCustomer Symptom Transformation in Progress', self.verbose)       
            data_cs = data[data['note_type_mod'] == 'CUSTOMER SYMPTOM']
            data_cs = data_cs[data_cs['note_type_mod'].notnull()]
            logdebug('Num Rows with Customer Symptoms: ' +str(data_cs.shape), self.verbose)
            data_cs['original_text'] = np.where(data_cs['notes'].isin(['None', None, '',' ','Please refer to the note detail', 'Please look at Note Details'])|
                                                  data_cs['notes'].isnull(), 
                                                  data_cs['notes_detail'], 
                                                  data_cs['notes'])
            data_cs['original_text'] = np.where(((data_cs['original_text'].isna()==True)|
                                                (data_cs['original_text'] == "None") | 
                                               (data_cs['original_text']=="na")),'', data_cs['original_text'] )
            data_cs['ext_text'] = data_cs['original_text'].apply(lambda x : self.feature_preprocessor.extract_prob_details(x))
            data_cs = data_cs[data_cs['ext_text']!=''].reset_index(drop = True)
            data_cs = data_cs[~data_cs['ext_text'].isna()].reset_index(drop = True)
            data_cs = data_cs[data_cs['ext_text']!="NA"].reset_index(drop = True)
            data_cs = data_cs[['incident_number','original_text','ext_text']].drop_duplicates(subset = ['incident_number'])
            
            # cleaning
            data_cs_cleaned = self.__data_clean_pp(data_cs,'ext_text',self.feature_preprocessor.clean_cust_symp)
            del data_cs
            data_cs_cleaned = data_cs_cleaned[['incident_number','ext_text','problem_desc_clean']]
            data_cs_cleaned.columns = ['incident_number','original_text', 'clean_text']
            id = list(range(0, len(data_cs_cleaned.index)))
            data_cs_cleaned['id'] = pd.Series(id).values
            data_cs_cleaned['original_text_SPLITSRBYTHIS'] = data_cs_cleaned["incident_number"] + "SPLITSRBYTHIS" +  data_cs_cleaned["original_text"]            
            data_cs_cleaned = data_cs_cleaned[['id', 'incident_number', 'original_text','original_text_SPLITSRBYTHIS', 'clean_text']]
            
            # feature extraction
            data_cs_cleaned['bugs'] = data_cs_cleaned['original_text'].apply(lambda x: self.feature_preprocessor.scrape_bugs(x))
            data_cs_cleaned['digital_signatures'] = data_cs_cleaned['original_text'].apply(lambda x: self.feature_preprocessor.scrape_signatures(x))
#             data_cs_cleaned['different_sr'] = data_cs_cleaned['original_text_SPLITSRBYTHIS'].apply(self.feature_preprocessor.scrape_sr_no)
            data_cs_cleaned['pid'] = data_cs_cleaned['original_text'].apply(self.feature_preprocessor.extract_pid)
            data_cs_cleaned['note_type'] = 'CUSTOMER_SYMPTOM'
            data_cs_cleaned.to_pickle(self.transformed_cs)
            del data_cs_cleaned 
            
        if problem_desc:
            
            logdebug('\n\n\nProblem Description Transformation in Progress', self.verbose)       
            data_pd = data[data['note_type_mod'] == 'PROBLEM DESCRIPTION']
            data_pd['creation_date'] = pd.to_datetime(data_pd['creation_date'])
            data_pd = data_pd.sort_values(by = ['incident_number','creation_date'], ascending = [False, False]).reset_index(drop = True)
            data_pd = data_pd.drop_duplicates('incident_number',keep='first')
            data_pd = data_pd[data_pd['note_type_mod'].notnull()]
            logdebug('Num Rows with Problem Description: ' +str(data_pd.shape), self.verbose)
            data_pd['original_text'] = np.where(data_pd['notes'].isin(['None', None, '',' ','Please refer to the note detail', 'Please look at Note Details'])|
                                                  data_pd['notes'].isnull(), 
                                                  data_pd['notes_detail'], 
                                                  data_pd['notes'])
            data_pd['original_text'] = np.where(((data_pd['original_text'].isna()==True)|
                                                (data_pd['original_text'] == "None") | 
                                               (data_pd['original_text']=="na")),'', data_pd['original_text'] )
            data_pd['ext_text'] = data_pd['original_text'].apply(lambda x : self.feature_preprocessor.extract_prob_details(x))
            data_pd = data_pd[data_pd['ext_text']!=''].reset_index(drop = True)
            data_pd = data_pd[~data_pd['ext_text'].isna()].reset_index(drop = True)
            data_pd = data_pd[data_pd['ext_text']!="NA"].reset_index(drop = True)
            data_pd = data_pd[['incident_number','original_text','ext_text']].drop_duplicates(subset = ['incident_number'])
            
            # cleaning
            data_pd_cleaned = self.__data_clean_pp(data_pd,'ext_text',self.feature_preprocessor.clean_prob_desc)
            del data_pd
#             print (data_pd_cleaned.columns)
            data_pd_cleaned = data_pd_cleaned[['incident_number','ext_text','problem_desc_clean']]
            data_pd_cleaned.columns = ['incident_number', 'original_text', 'clean_text']
            id = list(range(0, len(data_pd_cleaned.index)))
            data_pd_cleaned['id'] = pd.Series(id).values
            data_pd_cleaned['original_text_SPLITSRBYTHIS'] = data_pd_cleaned["incident_number"] + "SPLITSRBYTHIS" +  data_pd_cleaned["original_text"]           
            data_pd_cleaned = data_pd_cleaned[['id', 'incident_number', 'original_text','original_text_SPLITSRBYTHIS', 'clean_text']]
            
            # feature extraction
            data_pd_cleaned['bugs'] = data_pd_cleaned['original_text'].apply(lambda x: self.feature_preprocessor.scrape_bugs(x))
            data_pd_cleaned['digital_signatures'] = data_pd_cleaned['original_text'].apply(lambda x: self.feature_preprocessor.scrape_signatures(x))
#             data_pd_cleaned['different_sr'] = data_pd_cleaned['original_text_SPLITSRBYTHIS'].apply(self.feature_preprocessor.scrape_sr_no)
            data_pd_cleaned['pid'] = data_pd_cleaned['original_text'].apply(self.feature_preprocessor.extract_pid)
            data_pd_cleaned['note_type'] = 'PROBLEM DESCRIPTION'
            data_pd_cleaned.to_pickle(self.transformed_pd)
            del data_pd_cleaned
            
#         if resolution:
            
#             logdebug('\n\n\nResolution Transformation in Progress', self.verbose)       
#             data_res = data[data['note_type_mod'] == 'RESOLUTION SUMMARY']
#             data_res['creation_date'] = pd.to_datetime(data_res['creation_date'])
#             data_res = data_res.sort_values(by = ['incident_number','creation_date'], ascending = [False, False]).reset_index(drop = True)
#             data_res = data_res.drop_duplicates('incident_number',keep='first')
#             data_res = data_res[data_res['note_type_mod'].notnull()]
#             logdebug('Num Rows with Resolution: ' +str(data_res.shape), self.verbose)
#             data_res['original_text'] = np.where(data_res['notes'].isin(['None', None, '',' ','Please refer to the note detail', 'Please look at Note Details'])|
#                                                   data_res['notes'].isnull(), 
#                                                   data_res['notes_detail'], 
#                                                   data_res['notes'])
#             data_res['original_text'] = np.where(((data_res['original_text'].isna()==True)|
#                                                 (data_res['original_text'] == "None") | 
#                                                (data_res['original_text']=="na")),'', data_res['original_text'] )
            
#             data_res = data_res[data_res['original_text']!=''].reset_index(drop = True) 
#             data_res['resolution_tags'] = data_res['notes_concat'].apply(self.feature_preprocessor.get_resolution_tags) # what all res tags are present
            
            
#             std_tags = ['action plan', 'actions taken', 'problem summary', 'rca', 'resolution summary', 'solution', 'workaround']
#             for key in std_tags:
#                 data_res[key] = data_res['notes_concat'].apply(lambda x : self.feature_preprocessor.extract_tags_data(key,x))
            
#             # cleaning
#             data_res_cleaned = self.feature_preprocessor.data_clean_pp(data_res,'original_text',self.feature_preprocessor.clean_resolution)
#             data_res_cleaned = data_res_cleaned[['incident_number','original_text','resolution_clean']]
#             # cleaning for resolution tags
#             for i in std_tags:
#                 temp_df = data_clean_pp(data_res,str(i),self.feature_preprocessor.clean_resolution)
#                 data_res_cleaned[i+'_clean'] = temp_df['res_clean']
#             del data_res
            
#             data_res_cleaned['blank_flag'] = np.where(((data_res_cleaned['resolution_clean']=='')&(data_res_cleaned['action plan_clean']=='')&
#                                               (data_res_cleaned['actions taken_clean']=='')&(data_res_cleaned['problem summary_clean']=='')&
#                                             (data_res_cleaned['rca_clean']=='')&(data_res_cleaned['resolution summary_clean']=='')&
#                                               (data_res_cleaned['solution_clean']=='')&(data_res_cleaned['workaround_clean']=='')), 1,0)
            
#             data_res_cleaned = data_res_cleaned[data_res_cleaned['blank_flag']!=1].reset_index(drop = True)
            
#             data_res_cleaned = data_res_cleaned[['incident_number','resolution_clean','action plan_clean', 
#                                                  'actions taken_clean', 'problem summary_clean','rca_clean', 
#                                                  'resolution summary_clean', 'solution_clean','workaround_clean']]
            
#             id = list(range(0, len(data_res_cleaned.index)))
#             data_res_cleaned['id'] = pd.Series(id).values
#             data_res_cleaned['original_text_SPLITSRBYTHIS'] = data_res_cleaned["incident_number"] + "SPLITSRBYTHIS" +  data_res_cleaned["original_text"]           
            
#             # feature extraction
#             data_res_cleaned['bugs'] = data_res_cleaned['original_text'].apply(lambda x: self.feature_preprocessor.scrape_bugs(x))
#             data_res_cleaned['digital_signatures'] = data_res_cleaned['original_text'].apply(lambda x: self.feature_preprocessor.scrape_signatures(x))
#             data_res_cleaned['different_sr'] = data_res_cleaned['original_text_SPLITSRBYTHIS'].apply(self.feature_preprocessor.scrape_sr_no)
#             data_res_cleaned['pid'] = data_res_cleaned['original_text'].apply(self.feature_preprocessor.extract_pid)
#             data_res_cleaned['note_type'] = 'RESOLUTION SUMMARY'
#             data_res_cleaned.to_pickle(self.transformed_res)
#             del data_res_cleaned

            
        logdebug('All Extractions and Transformations Complete', self.verbose)
        if combine:
            logdebug('Reading Data Frames', self.verbose)
            try:
                data_summary = pd.read_pickle(self.transformed_summary)
                data_pd = pd.read_pickle(self.transformed_pd)
                data_cs = pd.read_pickle(self.transformed_cs)
#                 data_rs = pd.read_pickle(self.transformed_rs)
#                 data_email = pd.read_pickle(self.transformed_email)  
            except:
                logdebug('One or all of these data frames (Summary, PD, CS or RS ) not available at data path', self.verbose)


            logdebug('Concatenating Data Frames', self.verbose)
            required_cols = ['id', 'incident_number', 'original_text', 'clean_text', 'note_type', 'bugs', 'digital_signatures']
            data_final = pd.concat([data_summary[required_cols],
                                    data_pd[required_cols],
                                    data_cs[required_cols],
                                     ])
            logdebug('Concatenated Data Frame Shape: ' + str(data_final.shape), self.verbose)
#             merge_cols = ['incident_number', 'hybrid_product_family', 'tech_name','sub_tech_name','problem_code','resolution_code']
#             data = data[merge_cols].drop_duplicates(keep = 'last')
#             data_final = pd.merge(data_final, data,  on = 'incident_number')
            logdebug('Concatenated Merged Data Frame Shape: ' + str(data_final.shape), self.verbose)

            data_final = data_final[data_final['clean_text'].notnull()]
            data_final = data_final[data_final['clean_text']!='']
#             data_final = data_final[data_final['sub_tech_name'].isin(self.vpn_mobility_sub_techs)]
            logdebug(data_final.shape, self.verbose)
            logdebug('Data Transormation Done', self.verbose)

#             logdebug('If you want to Examine Raw and Orginal Text for: \n', self.verbose)
#             logdebug('PROBLEM_DESCRIPTION \n', self.verbose)
#             logdebug('CUSTOMER_SYMPTOM \n', self.verbose)
#             logdebug('RESOLUTION_SUMMARY \n', self.verbose)
#             logdebug('EMAIL_IN \n', self.verbose)
            logdebug('Saving Final Data Frame to Pickle', self.verbose)
            data_final.to_pickle(self.transform_file_path)   
            logdebug('Those data frames are written to the following location: ' + self.transform_file_path , self.verbose)
        
        return data_final       
        
    
