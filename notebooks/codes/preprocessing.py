import os
import re
import nltk
import string
import gensim
import datetime
import calendar
import itertools
import pycountry
import numpy as np
import collections
import pandas as pd
import seaborn as sns
from nltk import ngrams
from collections import Counter
import matplotlib.pyplot as plt
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
from nltk import sent_tokenize
from gensim import corpora, models, similarities
from gensim.corpora.dictionary import Dictionary
import multiprocessing
from multiprocessing.pool import Pool
import pickle
import warnings
warnings.filterwarnings("ignore", category=DeprecationWarning) 
pd.options.mode.chained_assignment = None  
pd.options.mode.chained_assignment = None  
from utility_functions import logdebug, preprocessing_support

preprocess_support = preprocessing_support()
import configparser
config = configparser.ConfigParser()
config.read('./config.ini')

# files_path = '/users/hdpqa/HadoopDir/TAC-Clustering/data/bi2i/anju_rohit/input/'
# output_files_path = '/users/hdpqa/HadoopDir/TAC-Clustering/data/bi2i/anju_rohit/output/Security_PF/'

class Preprocessing():
    def __init__(self):

        self.spell_correction = config['spell_check']['spell_check_dict']
        self.spell_correction_df = np.load(self.spell_correction, allow_pickle=True).item()
        self.preprocess_support = preprocess_support
        
    
    def extract_prob_details(self,text):
        text = text.replace('\n', 'NEWLINE')
    #     print (text)

        '''extract everything after Problem Details:'''
        pattern = 'problem details\:?(.*)'
        match = re.search(pattern, text, re.I)
        if match:
            matched_text = match.group(1)
            ext_text = matched_text.strip()
            final_text = ext_text.replace('NEWLINE', '\n').strip()
            return final_text
        else:
            text = text.replace('NEWLINE','\n')
            return text


    def extract_prob_description(self,text):
        text = text.replace('\n', 'NEWLINE')
        #remove non-ascii characters
        text = ''.join([letter for letter in text if ord(letter)<128])

        '''extract everything after Problem Description:'''

        pattern = 'problem description\:?(.*)|problem details\:?(.*)'
        match = re.findall(pattern, text, re.I)
        if len(match)>0:
            text_mod = ' '.join(list(match[0])).strip()
            final_text = text_mod.replace('NEWLINE', '\n').strip()
            if ((len(final_text) == 0) or (final_text == '')):
                return text
            else:
                return final_text

        else:
            text = text.replace('NEWLINE', '\n')
            return text  

    ## master function for stringent cleaning in subset data
    def clean_cust_symp(text):
        text = std_text(text)
        if "Original Message" in text:
            text_temp = text.split('Original Message')[1]
            text_temp = clean_salutation_end_phrases(text_temp)
            text_temp = clean_email(text_temp)
            cleaned_text = {'cust_symp_clean' : text_temp}
            return cleaned_text

        else:
            cleaned_text = clean_logs(text)
            cleaned_text = clean_question(cleaned_text) 
            cleaned_text = remove_lines_with_generic_text(cleaned_text)
            cleaned_text = remove_sent_with_generic_text(cleaned_text)
            cleaned_text = remove_markers(cleaned_text) 
            #cleaned_text = remove_generic_sent(cleaned_text)
            # cleaned_text = remove_stopwords(cleaned_text)
            cleaned_text = basic_cleaning(cleaned_text)
            #cleaned_text = remove_names(cleaned_text)
            cleaned_text = remove_extra_spaces(cleaned_text)
            cleaned_text = drop_dup_sent(cleaned_text)
            cleaned_text = {'cust_symp_clean' : cleaned_text}
            return cleaned_text
        
    def clean_prob_desc(text):
        text = std_text(text)
        if "Original Message" in text:
            text_temp = text.split('Original Message')[1]
            text_temp = clean_salutation_end_phrases(text_temp)
            text_temp = clean_email(text_temp)
            cleaned_text = {'problem_desc_clean' : text_temp}
            return cleaned_text

        else:
            cleaned_text = clean_logs(text)
            cleaned_text = clean_question(cleaned_text) 
            cleaned_text = remove_lines_with_generic_text(cleaned_text)
            cleaned_text = remove_sent_with_generic_text(cleaned_text)
            cleaned_text = remove_markers(cleaned_text) 
            #cleaned_text = remove_generic_sent(cleaned_text)
            # cleaned_text = remove_stopwords(cleaned_text)
            cleaned_text = basic_cleaning(cleaned_text)
            #cleaned_text = remove_names(cleaned_text)
            cleaned_text = remove_extra_spaces(cleaned_text)
            cleaned_text = drop_dup_sent(cleaned_text)
            cleaned_text = {'problem_desc_clean' : cleaned_text}
            return cleaned_text  
    
    def clean_resolution(text):
        cleaned_text = clean_logs(text)
        cleaned_text = clean_question(cleaned_text)
        cleaned_text = remove_lines_with_generic_text(cleaned_text)
        cleaned_text = basic_cleaning(cleaned_text)
        cleaned_text = strip_punctuations(cleaned_text)
        cleaned_text = remove_closure_lines(cleaned_text)
        cleaned_text = remove_extra_spaces(cleaned_text)
        cleaned_text = drop_dup_sent(cleaned_text)

        cleaned_text = {'resolution_clean' : cleaned_text}
        return cleaned_text

    ## one master function for cleaning summary 
    def clean_summary(text):

        # same case
        text = ' '.join(j.lower() for j in text.split()) 
        # non ascii
        text = ''.join([i for i in text if ord(i)<128])
        # remove hyperlinks
        text = ' '.join([re.sub(r'(http|https)?:\/\/(\w|\.|\/|\?|\=|\&|\%)*\b', ' ', i, flags=re.MULTILINE) 
                          for i in text.split()]) 

        #Remove Folder Paths Eg: /san1/EFR/sp_r53x_5_3_4/workspace/platforms/viking/fabric/drivers/arb/lib/lightning/src/lit_regio.c:565
        text = re.sub(r'\B/\w+\/{1}[\d\w-]+(\.[\d\w-]+)*(?:(?:\/[^\s/]*))*', ' ', text)

        # extra spaces 
        text = re.sub('\s+' ,' ', text)

        #Remove emails
        text = re.sub('\S*@\S*\s?',' ',text)

        # extra spaces 
        text = re.sub('\s+' ,' ', text)

        #Remove multiple instance of the repeating characters -=/_ Ex: Replace observation==== with observation
        regex  = re.compile('(\=\=+)|(\/\/+)|(\_\_+)|(\-\-+)') 
        text = re.sub(regex, "", text)

        #Remove repeating words fan fan tray = fan tray
        text = re.sub(r'\b(\w+)( \1\b)+', r'\1', text)

        # don't ": do not
        text = ' '.join([contractions_std(i) for i in text.split()])    

        # remove extra spaces
        text = re.sub('\s+', ' ', text).strip()

        #remove single occurence of alphabets sandwiched between spaces
        text = ' ' + text + ' '
        text = re.sub(' [A-z0-9] ', ' ', text)

        # remove extra spaces
        text = re.sub('\s+', ' ', text).strip()

        # remove . and ,
        text = re.sub('\.|\,' ,' ', text)
        #  print (text)

        text = re.sub('\s+' ,' ', text)
        #   print (text)

        # only valid alpha words 
        text = ' '.join([i for i in text.split() if i.isalpha() == True])

        # spell check 
        spell_corrected = []
        for i in text.split():
            if i in list(spell_correction_df.keys()):
                spell_corrected.append(spell_correction_df[i])
            else:
                spell_corrected.append(i)

        text = ' '.join(spell_corrected)

        # if all words are stop words remove line
        if all(j in stopwords_list for j in text.split())==False:
            text = remove_stopwords(text) # remove stopwords
            text = re.sub('\s+', ' ', text).strip()
        else:
            text = ''   


        # should have 2 or more words
        if len(text.split()) > 1 :
            text = {'summary_clean' : text}
        else:
            text = {'summary_clean' : ''}

        return text


    def scrape_bugs(text):
        text = text.replace('\n', 'NEWLINE')
        bugs_list = []
        BUG_PATTERN = r'(csc\w{2,2}\d{5,5})'
        bugs_detected = re.findall(BUG_PATTERN, text.lower())
        bugs_list.extend(bugs_detected)
        if len(bugs_list)==0:
            return ''
        else:
            return list(set(bugs_list))

    def scrape_signatures(text):
        text = text.replace('\n', 'NEWLINE')
        sig_list = []
    #     SIG_PATTERN = r'(%[\_\-A-z0-9\/\\]+)'
        SIG_PATTERN = r' (%[\_\-A-z0-9\/\\]+)'
        signatures_detected = re.findall(SIG_PATTERN, text.lower())
        sig_list.extend(signatures_detected)
        sig_list = [sig for sig in sig_list if len(sig) > 5]
        if len(sig_list)==0:
            return ''
        else:
            return list(set(sig_list))
       



    def scrape_sr_no(text): # check
        text = str(text)
        current_sr_no = text.split("SPLITSRBYTHIS")[0]
        text = text.split("SPLITSRBYTHIS")[1]
        text = text.replace('\n', 'NEWLINE')
        sr_no_list = []
        sr_pattern = r'6[\d]{8}'
        sr_detected = re.findall(sr_pattern, text)

        sr_no_list.extend([sr for sr in sr_detected if sr != str(current_sr_no)])

        if len(sr_no_list)==0:
            return ""
        else:
            return list(set(sr_no_list))

        
    def extract_pid(text):
        text = str(text)
        text = ' '+text+ ' '
        pat = r'[A-Z]{1}(?=[0-9\-])[A-Z0-9\-]{9,17}'
        pid_list = re.findall(pat,text)
        if len(pid_list)>0:
            return list(set(pid_list))
        else:
            return ''

    # Parallel processing for cleaning for w2v 
    def data_clean_pp(df,col,cleaning_function):

        processes_count = multiprocessing.cpu_count()-20
        print(processes_count)
        print("into parallel proc")

        final_df = pd.DataFrame()

        pool = Pool(processes = processes_count)
        temp_list = pool.map(cleaning_function,list(df[col]))
        interim_data = pd.DataFrame.from_dict(list(filter(None, temp_list)))
        interim_data['incident_number'] = df.incident_number
        interim_data[col] = df[col]
        final_df = pd.concat([final_df, interim_data])
        print('Finished cleaning')
        return final_df