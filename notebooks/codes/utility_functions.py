import os
import re
import nltk
import string
import datetime
import calendar
import pycountry
import numpy as np
import collections
import pandas as pd
from collections import Counter
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize, sent_tokenize
import multiprocessing
from multiprocessing.pool import Pool
import pickle
import warnings
import logging
warnings.filterwarnings("ignore", category=DeprecationWarning) 
pd.options.mode.chained_assignment = None 
pd.options.mode.chained_assignment = None
import configparser
config = configparser.ConfigParser()
config.read('./config.ini')

from time import time
import pandas as pd
import numpy as np
from gensim.models.fasttext import FastText
import nltk
nltk.download('stopwords')

# print runtime logs
def logdebug(message, verbose):
    logging.debug(message)
    if verbose > 0:
        print(message)
        
# all the support functions for preprocessing  
class preprocessing_support():
    def __init__(self):
        
        self.technology = config['Technology']['technology']
        self.stopwords_file = config['PD_Health']['stopwords']
        self.irrelavant_entities_file = config['PD_Health']['irrelavant_entities']
        self.email_end_detector_file = config['PD_Health']['email_end_detector']
        
        
        self.resolution_tags = config['res_health']['resolution_tags']
        self.closure_phrases_list = config['res_health']['closure_phrases']
        
        # if pre trained spell correction then use these
        self.spell_correction = config['spell_check']['spell_check_dict']
        self.spell_correction_df = np.load(self.spell_correction, allow_pickle=True).item()
        
    def clean_salutation_end_phrases(text):
        cleaned_text = std_text(text)
        cleaned_text = remove_salutations(cleaned_text) # update for spell check generic data
        cleaned_text = email_end_detector(cleaned_text)
        cleaned_text = {'cleaned_v1' : cleaned_text}
        return cleaned_text
    
    def remove_salutations(text):
        text_split = text.splitlines()
        new_text = []
        for line in text_split:
            line_temp =  re.sub('\.|\,|\!', '', line)
            line_split = line_temp.split()
            line_split = [' ' + x + ' ' for x in line_split]
            if any([x in salutation_words for x in line_split]):
                pass
            else:
                new_text.append(line)
        return '\n'.join(new_text)
    
    def email_end_detector(text):

        text = text.lower()
        text = text.replace('\n', 'NEWLINE')
        text = re.sub(pattern_email_end_detector ,' ',text, re.I)   
        text = text.replace('NEWLINE', '\n')
        return text
    
    def clean_email(text):
        cleaned_text = clean_logs(text)
        cleaned_text = clean_question(cleaned_text)
        cleaned_text = remove_sent_with_generic_text(cleaned_text)
        cleaned_text = remove_lines_with_generic_text(cleaned_text)
    #     cleaned_text = remove_generic_sent(cleaned_text)
    #     cleaned_text = remove_stopwords(cleaned_text)
        cleaned_text = basic_cleaning(cleaned_text)
        cleaned_text = strip_punctuations(cleaned_text)
    #     cleaned_text = remove_names(cleaned_text)
        cleaned_text = remove_extra_spaces(cleaned_text)
        cleaned_text = drop_dup_sent(cleaned_text)
        cleaned_text = {'cleaned_email' : cleaned_text}
        return cleaned_text
    
    def remove_extra_spaces(text):
        if text != None:
            return '\n'.join([re.sub('\s+', ' ', i).strip() for i in text.splitlines()])
        
    def drop_dup_sent(text):
        if text != None:
            x = text.splitlines()
            return '\n'.join(sorted(set(x), key = x.index))

    def remove_sent_with_generic_text(text):
    
        if text != None:
            line_list = []
            for line in text.splitlines():
                sent_list = []
                for sent in sent_tokenize(line):
                    sent_temp = sent.strip()
                    sent_temp = re.sub('[^A-Za-z\s]+', ' ', sent_temp).strip() # remove non alpha characters

                    if generic_check(sent_temp)=='':
                        pass
                    else:
                        sent_list.append(sent) 

                line_mod = ' '.join(sent_list)
                line_list.append(line_mod)
            final_text = '\n'.join([i for i in line_list if i!=''])
            return final_text
    
    def remove_lines_with_generic_text(text):
        if text != None:

            temp_list = []
            for line in text.splitlines():
                if line.upper().startswith(("CHEERS", "DISCLAIMER", "KIND REGARDS", "THANKS & ", "CUSTOMER SUPPORT ENGINEER",
                                                    "BR",  "OFFICE HOURS:", "WORK HOURS", "TEAM LEAD:", "MANAGER:", "BEST",
                                                    "WITH BEST REGARDS",
                                                    "MY BEST",  "MY BEST TO YOU", "ALL BEST", "ALL THE BEST", "BEST REGARDS",
                                                    "BESTS", "REGARDS", "REGARDS,", "REGARDS.", "THANKS AND", "RGDS", "WARM REGARDS", 
                                                    "WARMEST",
                                                    "WARMLY", "THANKS,",  "MANY THANKS", "THX,", "SINCERELY,", "CHEERS!", "KIND",
                                                    "CIAO", "YOURS TRULY", "VERY TRULY YOURS", "SENT FROM", ">",
                                                    'TO:', 'CC:', 'PHONE', 'EMAIL','CISCO HIGH TOUCH TECHNICAL SUPPORT',
                                                    'CISCO WORLDWIDE CONTACT', 'SENT:',' " <', "FROM:", "SUBJECT:","KINDLY", 
                                                    'DATE', 'DIRECT MANAGER', 'OFFICE HOURS', 'CCIE', 'TO UPDATE YOUR CASE')) \
                                                    | line.strip(string.punctuation).strip().upper().endswith(('.PNG', '.JPG', '.TXT', '.DAT', '.JPEG','.JPG','.XLS',
                                                                           '.XLSX','.DOC','.DOCX','.PDF')):
                            pass

                else:
                    temp_list.append(line)

            final_text = '\n'.join(temp_list)
            return final_text
    
    def clean_question(text):
        if text!=None:

            final_text = []
            for i in text.splitlines():
                if '?' in i:
                    # in a line, if sentence doesn't end with "?" then return sentence
                    i = ' '.join([list_element   for list_element in  sent_tokenize(i) if list_element.endswith('?')==False])
                    final_text.append(i)
                else:
                    final_text.append(i)  
            text = '\n'.join(final_text)
        #     print(text)
            return text
       
    def strip_punctuations(text):
        clean_text = []
        for i in text.splitlines():
            i = i.strip(string.punctuation)
            clean_text.append(i)
        cleaned_text = '\n'.join(clean_text)    
        return cleaned_text

    def basic_clean_text(self,text): 
        
        text = text.lower()
        text = re.sub(r'[^\x00-\x7F]+',' ', text)
        text = re.sub('\s+',' ', text)
        return text
   
    def remove_markers(text):
        if text != None:
            line_list = []
            for line in text.splitlines():
                sent_list = []
                for sent in sent_tokenize(line):
                    sent_temp = sent.strip()
                    sent_temp = re.sub('[^A-Za-z\s]+', ' ', sent_temp).strip() # remove non alpha characters
                    if re.search(markers_pattern, sent):
                        final_sent = ''
                    else:
                        sent_list.append(sent)

                line_mod = ' '.join(sent_list)
                line_list.append(line_mod)
#                 print (line_mod)
            final_text = '\n'.join([i for i in line_list if i!=''])
            return final_text


    def email_end_detector(self,text):
        # email end phrases 
        df_email_end_detector = pd.read_csv(self.email_end_detector_file, encoding = 'iso-8859-2')
        email_end_detector_list = list(map(lambda x: x.lower(), df_email_end_detector['email_end_detector']))
        email_end_detector_list = set(list(email_end_detector_list))
        
        for end_word in email_end_detector_list:
            pattern = 'NEWLINE'+ end_word +',?'+'NEWLINE.*'
            text = re.sub(pattern ,' ',text)
        text = text.replace('NEWLINE', '\n')
        return text
  
    def basic_cleaning(text):
        if text != None:

            text = clean_single_letter(text)

            # remove all strings except alpha
            text = '\n'.join([' '.join([j for j in i.split() if j.isalpha()]) for i in text.splitlines()])

            #Remove emails
            text = re.sub('\S*@\S*\s?',' ',text)

            #Remove Phone Numbers
            text = re.sub('\s[0-9]{1,3}\s[0-9]{1,3}\s[0-9]{1,4}', ' ', text) #eg. (408 325 9873)
            text = re.sub('\s[0-9]{1,3}\s[0-9]{1,3}\s[0-9]{1,3}\s[0-9]{1,4}', ' ', text) #eg. (1 408 325 9873)    

            #Remove IP addresses
            text = re.sub(r"\s\d{2,3}\.\d{2,3}\.\d{2,3}\.\d{2,3}", ' ', text)
            text = re.sub(r"\s\d{2,3}\.\d{2,3}\.\d{2,3}", ' ', text)
            text = re.sub(r"\s\d{2,3}\.\d{2,3}\.\d{2,4}", ' ', text)
            text = re.sub(r"\s\d{2,3}\.\d{2,3}", ' ', text)

            #Remove time formats Ex:  05:09:09
            text = re.sub(r'\s\d{1,2}:\d{1,2}:\d{1,2}', ' ', text) 

            #Remove date formats Ex:  05/09 ,   05/09/2014 05-09-2014 05-Jan-2014
            text = re.sub(r'\s\d{1,4}/\d{1,4}/\d{1,4}', ' ', text)
            text = re.sub(r'\s\d{1,4}-\d{1,4}-\d{1,4}', ' ', text)
            text = re.sub(r'\s\d{1,2}/\d{1,2}', ' ', text)
            #text = re.sub(r'\d{1,4}-\w{1,4}-\d{1,4}', ' ', text)

            #Remove time formats Ex:  05:09
            text = re.sub(r'\s\d{1,2}:\d{1,2}', ' ', text)  

            #Remove date formats Ex:  05-09
            text = re.sub(r'\s\d{1,2}-\d{1,2}', ' ', text)

            text =  re.sub(r'\b[A-z]{3} [A-z]{3} \d{1,2} \d{2,4}','', text)  # Wed Dec 31 1969, Mon Apr 18 2016
            text =  re.sub(r'\b\d{1,2}:\d{1,2}:[0-9.]{1,6}', '', text) #00:03:06.753
            text =  re.sub(r'\b\d{1,2}-[A-z]{3,9}-\d{1,4}','',text ) # 05-Jan-2014
            text =  re.sub(r'\b[A-z]{3} \d{1,2} \d{2,4}','', text)  # May 16 2016

            re.sub(pat_tz_abs,' ', text)

            # spell check 
            text_temp_list = []
            for i in text.splitlines():
                spell_corrected = []
                for j in i.split():
                    if j in list(spell_correction_df.keys()):
                        spell_corrected.append(spell_correction_df[j])
                    else:
                        spell_corrected.append(j)
                text_temp = ' '.join(spell_corrected)
                text_temp_list.append(text_temp)

            text = '\n'.join(text_temp_list)

            # line should have 4 or more words 
            text = '\n'.join([i for i in text.splitlines() if len(i.split())>3])

            return text
        
    # retain these punctuations : , . ? ' "
    def get_len_punc(self,text):
        punc = "\\".join(list('!#$%&\*+-/;<=>@[\\]^_`{|}~'))
        text = str(text)
        length = len(re.findall('[' + punc + ']', text))
        return length

    # drop duplicate sentences 
    def drop_dup_sent(self,text):
        x = text.splitlines()
        return '\n'.join(sorted(set(x), key = x.index))


    def contractions_std(self,text):
        contractions_dict = {"doesn't": 'does not',
        "isn't": 'is not',
        "it's": 'it is',
        "hasn't": 'has not',
        "hadn't": 'had not',
        "didn't": 'did not',
        "haven't": 'have not',
        "wouldn't": 'would not',
        "can't": 'cannot',
        "i've": 'i have',
        "i'am": 'i am',
        "i'm": 'i am',
        "i'll": 'i will',
        'hw': 'hardware',
        'sw': 'software'}

        contractions_dict_v2 = dict()

        for i,j in contractions_dict.items():
            a = ' ' + i + ' '
            b = ' ' + j + ' '
            contractions_dict_v2[a] = b
            
        text = ' '+ text + ' '
        for i, j in contractions_dict_v2.items():
            text = re.sub(i,j, text, flags = re.I)
        return text

    def std_text(text):
    
        # same case
        text = '\n'.join([' '.join([j.lower() for j in i.split()]) for i in text.splitlines()]) 
        # non ascii
        text = '\n'.join([''.join([j for j in i if ord(j)<128]) for i in text.splitlines()]) 
        # don't ": do not
        text = '\n'.join([contractions_std(i) for i in text.splitlines()])
        #remove brackets
        text = '\n'.join([re.sub('[(){}\[\]]', ' ', i) for i in text.splitlines()])
        # remove hyperlinks
        text = '\n'.join([re.sub(r'(http|https)?:\/\/(\w|\.|\/|\?|\=|\&|\%)*\b', ' ', i, flags=re.MULTILINE) 
                          for i in text.splitlines()]) 
        #Remove Folder Paths Eg: /san1/EFR/sp_r53x_5_3_4/workspace/platforms/viking/fabric/drivers/arb/lib/lightning/src/lit_regio.c:565
        text = '\n'.join([re.sub(r'\B/\w+\/{1}[\d\w-]+(\.[\d\w-]+)*(?:(?:\/[^\s/]*))*', ' ', i, flags=re.MULTILINE) 
                          for i in text.splitlines()]) 
        #Remove multiple instance of the repeating characters -=/_ Ex: Replace observation==== with observation
        regex  = re.compile('(\=\=+)|(\/\/+)|(\_\_+)|(\-\-+)') 
        text = '\n'.join([re.sub(regex, '', i) for i in text.splitlines()]) 
        #Remove repeating words fan fan tray = fan tray
        text = '\n'.join([re.sub(r'\b(\w+)( \1\b)+', r'\1', i) for i in text.splitlines()]) 
        # remove extra spaces
        text = '\n'.join([re.sub('\s+', ' ', i).strip() for i in text.splitlines()])
        return text
    
    def case_std(self,text):
        # same case
        text = '\n'.join([' '.join([j.lower() for j in i.split()]) for i in text.splitlines()]) 
        # non ascii
        text = '\n'.join([''.join([j for j in i if ord(j)<128]) for i in text.splitlines()]) 
        # don't ": do not
        text = '\n'.join([self.contractions_std(i) for i in text.splitlines()])
        # remove hyperlinks
        text = '\n'.join([re.sub(r'(http|https)?:\/\/(\w|\.|\/|\?|\=|\&|\%)*\b', ' ', i, flags=re.MULTILINE) 
                          for i in text.splitlines()]) 

        #Remove Folder Paths Eg: /san1/EFR/sp_r53x_5_3_4/workspace/platforms/viking/fabric/drivers/arb/lib/lightning/src/lit_regio.c:565
        text = '\n'.join([re.sub(r'\B/\w+\/{1}[\d\w-]+(\.[\d\w-]+)*(?:(?:\/[^\s/]*))*', ' ', i, flags=re.MULTILINE) 
                          for i in text.splitlines()]) 

        #Remove multiple instance of the repeating characters -=/_ Ex: Replace observation==== with observation
        regex  = re.compile('(\=\=+)|(\/\/+)|(\_\_+)|(\-\-+)') 
        text = '\n'.join([re.sub(regex, '', i) for i in text.splitlines()]) 


        #Remove repeating words fan fan tray = fan tray
        text = '\n'.join([re.sub(r'\b(\w+)( \1\b)+', r'\1', i) for i in text.splitlines()]) 

        # remove extra spaces
        text = '\n'.join([re.sub('\s+', ' ', i).strip() for i in text.splitlines()])

        return text
    
    def strip_punctuations(text):
        clean_text = []
        for i in text.splitlines():
            i = i.strip(string.punctuation)
            clean_text.append(i)
        cleaned_text = '\n'.join(clean_text)    
        return cleaned_text 


    def remove_stopwords(self,text):
        df_stopwords = pd.read_csv(self.stopwords_file, encoding = 'iso-8859-2')
        stopwords_list = list(df_stopwords['stopwords'])
        stopwords_list = set(list(map(lambda x: x.lower().strip(), stopwords_list)))
        text = ' '+text +' '
        text = ' '.join([i for i in text.split() if i not in stopwords_list])
        text = re.sub('\s+',' ',text).strip()
        return text
    
    def remove_generic_sent(self,text):
        # create a list 
        df_stopwords = pd.read_csv(self.stopwords_file, encoding = 'iso-8859-2')
        stopwords_list = list(df_stopwords['stopwords'])
        stopwords_list = set(list(map(lambda x: x.lower().strip(), stopwords_list)))
        cleaned_text = '\n'.join([i for i in text.splitlines() if all(j in stopwords_list for j in i.split())==False])
        return text
    
    # defone next stop markers list 
    def clean_next_stop_markers(self,text):
        next_stop_markers_list = ["action plan",
        "business impact",
        "current status:",
        "actions taken",
        "troubleshooting done",
        "regards",
        "action done",
        "current configuration",
        "best regards",
        "sh log",
        "troubleshooting performed",
        "confidentiality notice:",
        "technical support:",
        "start date:"]

        text = text.replace('\n', 'NEWLINE')
        for key in next_stop_markers_list: # next stop markers 
            pattern = 'NEWLINE'+ key +'\s?:?.*'
            text = re.sub(pattern ,' ',text,re.I) # replace everything after pat
        text = text.replace('NEWLINE', '\n')
        return text
    

    def remove_line_with_text(self,text):

        # irrelavant entities
        df_irrelavant_entities = pd.read_csv(self.irrelavant_entities_file, encoding = 'iso-8859-2', index_col = [0])
        df_irrelavant_entities.columns = ['removal_entities','source']
        df_irrelavant_entities['removal_entities'] = df_irrelavant_entities['removal_entities'].map(self.basic_clean_text)

        remove_line_with_text_list = list(df_irrelavant_entities[df_irrelavant_entities['source']=="next_stop_marker"]['removal_entities'])
        remove_line_with_text_list = remove_line_with_text_list + ["think before you print"] # add in main file

        text_mod = []
        for i in text.splitlines():
            if any(j in i for j in remove_line_with_text_list):
                pass
            else:
                text_mod.append(i)
        text = '\n'.join(text_mod)
        return text
 
    def clean_logs(text):
        if text != None:

            ## remove paras with lines > 10
            # retain only those paras having len < 10
            m = [] # list to append paras  
            for i in text.split('\n\n'):
                if len(i.splitlines())>5: # if its a para having more than 5 lines
                    for j in i.splitlines():
                        if len([k for k in j.split() if k.isalpha()])>5:
                            m.append(j) # append the line if it has 6 or more valid words
                        else:
                            pass
                else:
                    m.append(i) # if the para has 5 or less lines then append the entire para

            text = '\n'.join(m)

    #         print (text)
    #         print ('-----------')

            # remove lines having ':' and less than 5 words
            temp_list = []
            for i in text.splitlines():
                if (":" in i) and (len([j for j in i.split() if j.isalpha() == True])<5):
                    pass
                else:
                    temp_list.append(i)        
            text = '\n'.join(temp_list) 

    #         print (text)
    #         print ('-----------')

            m = []
            for i in text.splitlines():
                # atleast 2 sent and each sent has atleast 4 words valid
                if len(sent_tokenize(i))>1: # if the line contains more than 2 or more sentences
                # no punc check required
                # print (sent_tokenize(i))
                    c = 0
                    for j in sent_tokenize(i):
                        if len([k for k in j.split() if re.sub('\.|\,','',k).isalpha()==True])>3: # if each sent has 4 or more valid words
                            c = c+1
                    if c>0:
                        m.append(i)
                    text = '\n'.join(m)

                else:
                    if  get_len_punc(i)<=5:
                        m.append(i)
                    elif ((get_len_punc(i)>5) & (len([j for j in i.split() if re.sub('\.|\,','',j).isalpha()])>=10)):
                        m.append(i)
                    else:
                        m.append(' ')
            text = '\n'.join(m)
            return text   
        
    def include_spell_mistake(self, word, similar_word, score):
        edit_distance_threshold = 1 if len(word) <= 4 else 2
        return (score > fasttext_min_similarity
                and len(similar_word) > 3
                and vocab[similar_word] >= spell_mistake_min_frequency
                and word[0] == similar_word[0]
                and nltk.edit_distance(word, similar_word) <= edit_distance_threshold)
    
    def train_spell_check(self,data_summ, data_cs, data_pd):
        data_summ_cleaned_final = pd.read_pickle(config[file_paths]['Transformed_Summary'])
        data_summ_cleaned_final.rename(columns = {'clean_text': 'summary'},inplace = True)
        data_cs_cleaned_final = pd.read_pickle(config[file_paths]['Transformed_CS'])
        data_cs_cleaned_final.rename(columns = {'clean_text': 'custsymp'},inplace = True)
        data_pd_cleaned_final = pd.read_pickle(config[file_paths]['Transformed_PD'])
        data_pd_cleaned_final.rename(columns = {'clean_text': 'probdesc'},inplace = True)

        data = pd.concat([data_summ_cleaned_final,data_cs_cleaned_final,data_pd_cleaned_final])

        attribute_list = ['summary','custsymp','probdesc']
        for i in attribute_list:
            for line in data['i']:
                line = str(line).lower().strip()
                all_doc.append(line.split())
                words.extend(line.split())

        vocab = collections.Counter(words)
        modelFT = FastText(size=100, window=3, min_count=1, sentences=all_doc, min_n =3,max_n=6,min_alpha=0.01 )
        spell_mistake_min_frequency = 2
        fasttext_min_similarity = 0.96
        word_to_mistakes = collections.defaultdict(list)
        nonalphabetic = re.compile(r'[^a-zA-Z]')
        for word, freq in vocab.items():
            if freq < 500 or len(word) <= 3 or nonalphabetic.search(word) is not None:
                #  To keep this task simple, we will not try finding
                #  spelling mistakes for words that occur less than 500 times
                #  or have length less than equal to 3 characters
                #  or have anything other than English alphabets
                continue

            # Query the fasttext model for 50 closest neighbors to the word
            similar_words = modelFT.wv.most_similar(word, topn=50)
            for results in similar_words :
                if self.include_spell_mistake(word, similar_word= results[0], score=results[1]):
                    word_to_mistakes[word].append(results[0])

        inverted_index_spel_check = {}
        for word, mistakes in word_to_mistakes.items():
            for mistake in mistakes:
                if mistake != word:
                    inverted_index_spel_check[mistake] = word

        np.save(config['spell_check']['spell_check_dict'], dict(spell_correction_df))

        
    def clean_question(self,text):
        final_text = []
        for i in text.splitlines():
            if '?' in i:
                # in a line, if sentence doesn't end with "?" then return sentence
                i = ' '.join([list_element   for list_element in  sent_tokenize(i) if list_element.endswith('?')==False])
                final_text.append(i)
            else:
                final_text.append(i)  
        text = '\n'.join(final_text)
        return text  

    def clean_pd(self,text, spell_check = True):      
        # lines with more than 5 puncs
        m = []
        for i in text.splitlines():
            i = re.sub('\s+',' ',i) # remove extra spaces

            # atleast 2 sent and each sent has atleast 4 words valid
            if len(sent_tokenize(i))>1:# no punc check required
        #         print (sent_tokenize(i))
                c = 0
                for j in sent_tokenize(i):
                    if len([k for k in j.split() if re.sub('\.|\,','',k).isalpha()==True])>3: # if each sent has 4 or more valid words
                        c = c+1
                if c>0:
                    m.append(i)
                text = '\n'.join(m)

            else:
                if  self.get_len_punc(i)<=5:
                    m.append(i)
                elif ((self.get_len_punc(i)>5) & (len([j for j in i.split() if re.sub('\.|\,','',j).isalpha()])>=5)):
                    m.append(i)
                else:
                    m.append(' ')
                text = '\n'.join(m)
    #   

        text = '\n'.join([re.sub("\.|\,|\:|\'|\"|\?",' ', i).strip() for i in text.splitlines()])

        # remove all strings except alpha
        text = '\n'.join([' '.join([j for j in i.split() if j.isalpha()]) for i in text.splitlines()])

        # spell check 
        if spell_check:

            text_temp_list = []
            for i in text.splitlines():
                spell_corrected = []
                for j in i.split():
                    if j in list(self.spell_correction_df.keys()):
                        spell_corrected.append(self.spell_correction_df[j])
                    else:
                        spell_corrected.append(j)
                text_temp = ' '.join(spell_corrected)
                text_temp_list.append(text_temp)

            text = '\n'.join(text_temp_list)
        else:
            # train if spell_check = False
            data_summ_cleaned_final = pd.read_pickle(config[file_paths]['Transformed_Summary'])
            data_summ_cleaned_final.rename(columns = {'clean_text': 'summary'},inplace = True)
            data_cs_cleaned_final = pd.read_pickle(config[file_paths]['Transformed_CS'])
            data_cs_cleaned_final.rename(columns = {'clean_text': 'custsymp'},inplace = True)
            data_pd_cleaned_final = pd.read_pickle(config[file_paths]['Transformed_PD'])
            data_pd_cleaned_final.rename(columns = {'clean_text': 'probdesc'},inplace = True)
            spell_correction_df = np.load(train_spell_check(self,data_summ_cleaned_final,data_cs_cleaned_final,data_pd_cleaned_final), allow_pickle=True).item()
            
            text_temp_list = []
            for i in text.splitlines():
                spell_corrected = []
                for j in i.split():
                    if j in list(self.spell_correction_df.keys()):
                        spell_corrected.append(self.spell_correction_df[j])
                    else:
                        spell_corrected.append(j)
                text_temp = ' '.join(spell_corrected)
                text_temp_list.append(text_temp)

            text = '\n'.join(text_temp_list)
            
        # line should have 4 or more words 
        text = '\n'.join([i for i in text.splitlines() if len(i.split())>3])

        return text
    
    # extract list of tags prsent in resolution raw text
    def get_resolution_tags(self,text): 
        resolution_tags = pd.read_csv(self.resolution_tags)
        res_summ_tag_pat = ''
        for i in resolution_tags['values']:
            res_summ_tag_pat = res_summ_tag_pat + 'NEWLINE'+ i +'|'
        res_summ_tag_pat = res_summ_tag_pat[:-1]

        text = text.replace('\n', 'NEWLINE')
        text = 'NEWLINE'+text
        if len([i[7:] for i in re.findall(res_summ_tag_pat, text,re.I)])>0:
            return [i[7:] for i in re.findall(res_summ_tag_pat, text,re.I)]
        else:
            return ''
        
    def extract_tags_data(self,key,text):
    
        # key : values of tags clubbed together
        std_tags = resolution_tags.groupby("key")["values"].agg(list).to_dict()

        key_level_ext_text = []
        for i in std_tags[key]:
            text = text.replace('\n', 'NEWLINE')
            text = 'NEWLINE'+text
            pattern = 'NEWLINE' + str(i) + r'\s?\:?\s*(.*?)(:?NEWLINE[A-z\s]+\s?:\s*)'  + '|' + 'NEWLINE' +  str(i) + r'\s?\:?\s*(.*?)(:?NEWLINE[A-z\s]+\s?:\s*)?$'
            found_pattern = re.findall(pattern, text, re.I)
            if len(found_pattern)==0: 
                extracted_text=''
            else:
                temp_list = list(found_pattern[0])
                l = [i for i in temp_list if len(i)!=0 and (temp_list.index(i)==0 or temp_list.index(i)==2)]
                if len(l)==0:
                    extracted_text = ''
                else:
                    extracted_text = max(l, key=len)

            if len(extracted_text)!=0:
                key_level_ext_text.append(extracted_text)
            else:
                key_level_ext_text.append('')

        return ' '.join([j for j in key_level_ext_text if j!='']).replace('NEWLINE','\n')
    
    
    def remove_closure_lines(text):   
        # add in config.ini

        clean_text_list = []
        for i in text.splitlines():
    #         print (i)
    #         print ('---------------------------------')
            i_mod_list = []
            for j in sent_tokenize(i):
    #             print (j)
    #             print ('---------------------------------')

                if (any(k in j.lower() for k in closure_phrases_list) == True) & (len(j.split())<8):
                    pass
                else:
                    i_mod_list.append(j)
            i_mod = ' '.join(i_mod_list)
            clean_text_list.append(i_mod.strip())     
        clean_text = '\n'.join(clean_text_list)
        return clean_text

