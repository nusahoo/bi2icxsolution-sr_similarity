#!/usr/bin/env python
# coding: utf-8

# In[2]:


import datetime as d
import multiprocessing
# from multiprocessing.pool import Pool
from functools import partial
from contextlib import contextmanager
import pandas as pd
import configparser
config = configparser.ConfigParser()
config.read('config.ini')
file_paths = 'input_file_paths'
from itertools import product
print ("packages imported")

# In[3]:


@contextmanager
def poolcontext(*args, **kwargs):
    pool = multiprocessing.Pool(*args, **kwargs)
    yield pool
    pool.terminate()
def data_clean_pp(df,col,cleaning_function, flags):

    processes_count = multiprocessing.cpu_count()-4
    print(processes_count)
    print("into parallel proc")

    final_df = pd.DataFrame()

    pool = Pool(processes = processes_count)
    temp_list = pool.map(cleaning_function,list(df[col]))
    interim_data = pd.DataFrame.from_dict(list(filter(None, temp_list)))
    interim_data['incident_number'] = list(df.incident_number)
    interim_data[col] = list(df[col])
    final_df = pd.concat([final_df, interim_data])
    print('Finished cleaning')
    return final_df

def absolute_preprocessor(data, attribute):
    if attribute == "summary":
        clean_data = data_clean_pp(data, "summary", clean_summary)
    if attribute == "prob_desc":
        clean_data = data_clean_pp(data, "problem_description", clean_prob_desc)
    if attribute == "cust_symp":
        clean_data = data_clean_pp(data, "customer_symptom", clean_prob_desc)
    if attribute == "res_summ":
        clean_data = data_clean_pp(data, "resolution_summary", clean_resolution)
    if attribute == "email":
        clean_data = data_clean_pp(data, "email", clean_salutation_end_phrases)
        clean_data = data_clean_pp(clean_data, "cleaned_v1", clean_email)
        
    return clean_data
def clean_prob_desc(text, flags={'log':1, 'ques':1, 'stopword':1}):
    print ("In clean_prob_desc")
    text = std_text(text)
#     if "original message" in text:
#         text_temp = text.split('original message')[1]
#         text_temp = clean_salutation_end_phrases(text_temp)
#         text_temp = clean_email(text_temp)
#         cleaned_text = {'problem_desc_clean' : text_temp}
#         return cleaned_text

#     else:
    if flags['log']:
        cleaned_text = clean_logs(text)
#     cleaned_text = clean_question(cleaned_text) 
    cleaned_text = remove_lines_with_generic_text(cleaned_text)
#     cleaned_text = remove_sent_with_generic_text(cleaned_text)
#     cleaned_text = remove_markers(cleaned_text) 
#     cleaned_text = remove_generic_sent(cleaned_text)
#     cleaned_text = remove_stopwords(cleaned_text)
#     cleaned_text = basic_cleaning(cleaned_text)
#     cleaned_text = strip_punctuations(cleaned_text)
#     cleaned_text = remove_names(cleaned_text)
#     cleaned_text = remove_extra_spaces(cleaned_text)
#     cleaned_text = drop_dup_sent(cleaned_text)
    cleaned_text = {'problem_desc_clean' : cleaned_text}
    return cleaned_text    
def clean_logs(text):
    if text != None:

        ## remove paras with lines > 10
        # retain only those paras having len < 10
        m = [] # list to append paras  
        for i in text.split('\n\n'):
            if len(i.splitlines())>5: # if its a para having more than 5 lines
                for j in i.splitlines():
                    if len([k for k in j.split() if k.isalpha()])>5:
                        m.append(j) # append the line if it has 6 or more valid words
                    else:
                        pass
            else:
                m.append(i) # if the para has 5 or less lines then append the entire para

        text = '\n'.join(m)

#         print (text)
#         print ('-----------')

        # remove lines having ':' and less than 5 words
        temp_list = []
        for i in text.splitlines():
            if (":" in i) and (len([j for j in i.split() if j.isalpha() == True])<5):
                pass
            else:
                temp_list.append(i)        
        text = '\n'.join(temp_list) 

#         print (text)
#         print ('-----------')

        m = []
        for i in text.splitlines():
            # atleast 2 sent and each sent has atleast 4 words valid
            if len(sent_tokenize(i))>1: # if the line contains more than 2 or more sentences
            # no punc check required
            # print (sent_tokenize(i))
                c = 0
                for j in sent_tokenize(i):
                    if len([k for k in j.split() if re.sub('\.|\,','',k).isalpha()==True])>3: # if each sent has 4 or more valid words
                        c = c+1
                if c>0:
                    m.append(i)
                text = '\n'.join(m)

            else:
                if  get_len_punc(i)<=5:
                    m.append(i)
                elif ((get_len_punc(i)>5) & (len([j for j in i.split() if re.sub('\.|\,','',j).isalpha()])>=10)):
                    m.append(i)
                else:
                    m.append(' ')
        text = '\n'.join(m)
        return text

def remove_lines_with_generic_text(text):
    if text != None:
        
        temp_list = []
        for line in text.splitlines():
            if line.upper().startswith(("CHEERS", "DISCLAIMER", "KIND REGARDS", "THANKS & ", "CUSTOMER SUPPORT ENGINEER",
                                                "BR",  "OFFICE HOURS:", "WORK HOURS", "TEAM LEAD:", "MANAGER:", "BEST",
                                                "WITH BEST REGARDS",
                                                "MY BEST",  "MY BEST TO YOU", "ALL BEST", "ALL THE BEST", "BEST REGARDS",
                                                "BESTS", "REGARDS", "REGARDS,", "REGARDS.", "THANKS AND", "RGDS", "WARM REGARDS", 
                                                "WARMEST",
                                                "WARMLY", "THANKS,",  "MANY THANKS", "THX,", "SINCERELY,", "CHEERS!", "KIND",
                                                "CIAO", "YOURS TRULY", "VERY TRULY YOURS", "SENT FROM", ">",
                                                'TO:', 'CC:', 'PHONE', 'EMAIL','CISCO HIGH TOUCH TECHNICAL SUPPORT',
                                                'CISCO WORLDWIDE CONTACT', 'SENT:',' " <', "FROM:", "SUBJECT:","KINDLY", 
                                                'DATE', 'DIRECT MANAGER', 'OFFICE HOURS', 'CCIE', 'TO UPDATE YOUR CASE')) \
                                                | line.strip(string.punctuation).strip().upper().endswith(('.PNG', '.JPG', '.TXT', '.DAT', '.JPEG','.JPG','.XLS',
                                                                       '.XLSX','.DOC','.DOCX','.PDF')):
                        pass

            else:
                temp_list.append(line)

        final_text = '\n'.join(temp_list)
        return final_text


# In[4]:

print ("functions defined!")
# reading file
data_pd = pd.read_pickle(config[file_paths]['probdesc'])


# In[ ]:


a = d.datetime.now()
probdesc = list(data_pd['problem_description'])
print ('1')
with poolcontext(processes=4) as pool:
    print ('2')
    results = pool.map(partial(clean_prob_desc, flags={'log':1, 'ques':1, 'stopword':1}), probdesc)
    print ("3")
b = d.datetime.now()
print (a-b)

print ("preproc done using multiprocessing!")


import multiprocessing
from itertools import product

def merge_names(a, b):
    return '{} & {}'.format(a, b)

# if __name__ == '__main__':
names = ['Brown', 'Wilson', 'Bartlett', 'Rivera', 'Molloy', 'Opie']
with multiprocessing.Pool(processes=3) as pool:
    results = pool.starmap(merge_names, product(names, repeat=2))
print(results)

print ("multiprocessing done!")



