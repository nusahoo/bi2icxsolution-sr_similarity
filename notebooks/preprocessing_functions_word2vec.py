import os
import re
import ast
import nltk
import string
import random
import pickle
import datetime
import calendar
import warnings
import itertools
import pycountry
import numpy as np
import collections
# import pycountry
import pandas as pd
import configparser
import seaborn as sns
import multiprocessing
from nltk import ngrams
from itertools import chain
from nltk import sent_tokenize
from collections import Counter
import matplotlib.pyplot as plt
from nltk.corpus import stopwords
from gensim.models import Word2Vec
from multiprocessing.pool import Pool
from nltk.tokenize import word_tokenize
from nltk.stem import WordNetLemmatizer
pd.options.mode.chained_assignment = None 
pd.options.mode.chained_assignment = None  
pd.options.mode.chained_assignment = None 
from sklearn.feature_extraction.text import TfidfVectorizer
# config file
config = configparser.ConfigParser()
config.read('config.ini')
file_paths = 'input_file_paths'
clean_file_paths = 'output_file_paths'

warnings.filterwarnings("ignore", category=DeprecationWarning) 
input_files_path = 'C:\\Users\\nusahoo\\Documents\\CustomerSolutions\\data\\input\\'

print ("Packages Imported!")


def absolute_preprocessor(data, attribute):
    if attribute == "summary":
        clean_data = data_clean_pp(data, "summary", clean_summary)
    if attribute == "prob_desc":
        clean_data = data_clean_pp(data, "problem_description", clean_prob_desc)
    if attribute == "cust_symp":
        clean_data = data_clean_pp(data, "customer_symptom", clean_prob_desc)
    if attribute == "res_summ":
        clean_data = data_clean_pp(data, "resolution_summary", clean_resolution)
    if attribute == "email":
        clean_data = data_clean_pp(data, "email", clean_salutation_end_phrases)
        clean_data = data_clean_pp(clean_data, "cleaned_v1", clean_email)
        
    return clean_data

# extract cs
def ext_pd_from_email(text):    
    text = re.sub('\n+', '\n', text)
    text = re.sub('=+', '', text)
    text = text.replace('\n', 'NEWLINE')
    text = 'NEWLINE'+text
    
    if re.search('Problem DescriptionNEWLINE', text, flags=re.I):
        pattern = r'NEWLINEProblem Description(.*?)NEWLINE[A-z]+\s[A-z]+NEWLINE|NEWLINEproblem description(.*)NEWLINE'
        found_pattern = re.findall(pattern, text, flags=re.I)
        if len(found_pattern)==0: 
            extracted_text=''
        else:
            temp_list = list(found_pattern[0])
            l = [i for i in temp_list if len(i)!=0]
            if len(l)==0:
                extracted_text=''
            else:
                extracted_text = max(l, key=len)
    else:
        i = "problem description"
        pattern = 'NEWLINE' + str(i) + r'\s?\:?\s*(.*?)(:?NEWLINE[A-z\s]+\s?:\s*)'  + '|' + 'NEWLINE' +  str(i) + r'\s?\:?\s*(.*?)(:?NEWLINE[A-z\s]+\s?:\s*)?$'
        found_pattern = re.findall(pattern, text, flags=re.I)
        if len(found_pattern)==0: 
                    extracted_text=''
        else:
            temp_list = list(found_pattern[0])
            l = [i for i in temp_list if len(i)!=0 and (temp_list.index(i)==0 or temp_list.index(i)==2)]
            if len(l)==0:
                extracted_text = ''
            else:
                extracted_text = max(l, key=len)
    return extracted_text.replace("NEWLINE", "\n")

def extract_prob_details(text):
    text = text.replace('\n', 'NEWLINE')
 
    '''extract everything after Problem Details:'''
    
    pattern = 'problem details:(.*)'
    match = re.search(pattern, text, re.I)
    if match:
        matched_text = match.group(1)
        ext_text = matched_text.strip()
        final_text = ext_text.replace('NEWLINE', '\n').strip()
        return final_text
    else:
        text = text.replace('NEWLINE','\n')
        return text
    
# extract pd
def extract_prob_description(text):
    text = text.replace('\n', 'NEWLINE')
    #remove non-ascii characters
    text = ''.join([letter for letter in text if ord(letter)<128])
    
    '''extract everything after Problem Description:'''
    
    pattern = 'problem description:?(.*)|problem details:(.*)'
    match = re.findall(pattern, text, re.I)
    if len(match)>0:
        text_mod = ' '.join(list(match[0])).strip()
        final_text = text_mod.replace('NEWLINE', '\n').strip()
        if ((len(final_text) == 0) or (final_text == '')):
            return text
        else:
            return final_text
        
    else:
        text = text.replace('NEWLINE', '\n')
        return text  
  
    ## remove email end signatures
df_email_end_detector = pd.read_csv(input_files_path + 'email_end_detector.csv', encoding = 'iso-8859-2')
df_email_end_detector['value'] = np.where(df_email_end_detector['value'].isna() , '', df_email_end_detector['value'])
phrases_group = df_email_end_detector.groupby('key')['value'].apply(list).reset_index(name='value')  
pattern_list = [ 'NEWLINE' + i + '.*?' +'(' + '|'.join(j) +')' + '\.?\,?\!?\;?' +'NEWLINE.*' for i,j in zip(phrases_group['key'].tolist(), phrases_group['value'].tolist())]
pattern_email_end_detector = '|'.join(pattern_list)

def email_end_detector(text):

    text = text.lower()
    text = text.replace('\n', 'NEWLINE')
    text = re.sub(pattern_email_end_detector ,' ',text, re.I)   
    text = text.replace('NEWLINE', '\n')
    return text


contractions_dict = {"doesn't": 'does not',
 "isn't": 'is not',
 "it's": 'it is',
 "hasn't": 'has not',
 "hadn't": 'had not',
 "didn't": 'did not',
 "haven't": 'have not',
 "wouldn't": 'would not',
 "can't": 'cannot',
 "i've": 'i have',
 "i'am": 'i am',
 "i'm": 'i am',
 "i'll": 'i will',
 'hw': 'hardware',
 'sw': 'software'}
 
contractions_dict_v2 = dict()
for i,j in contractions_dict.items():
    a = ' ' + i + ' '
    b = ' ' + j + ' '
    contractions_dict_v2[a] = b

def contractions_std(text):
    text = ' '+ text + ' '
    for i, j in contractions_dict_v2.items():
        text = re.sub(i,j, text, flags = re.I)
    return text

# create a list 
# df_stopwords = pd.read_csv(input_files_path + 'stop_words.csv', encoding = 'iso-8859-2')
# df_stopwords = df_stopwords[df_stopwords['source'].isin(['analyst','pycountry','nltk','analyst'])].reset_index(drop = True)
# df_stopwords = df_stopwords[df_stopwords['remove_flag']==0].reset_index(drop = True)
# stopwords_list = list(df_stopwords['stopwords'])
# stopwords_list = set(list(map(lambda x: x.lower().strip(), stopwords_list))) 

stopwords_list = pickle.load(open(input_files_path+"stopwords_list.p", "rb")) 
def remove_stopwords(text):
    
    if text != None:
        text = text.replace('\n','NEWLINE ')
        text = ' '+ text +' '
        text = ' '.join([i for i in text.split() if (re.sub('\.|\,','',i) not in stopwords_list)])
        text = re.sub('\s+',' ',text).strip()
        text = text.replace('NEWLINE ', '\n')
        return text
          
def remove_generic_sent(text):
    
    if text != None:
        # create a list 
        cleaned_text = '\n'.join([i for i in text.splitlines() if (all(j in stopwords_list for j in i.split())==False)])
        
        return text

        
salutation_words_df = pd.read_csv(input_files_path + 'salutation_list.csv')
salutation_words = [' ' + x + ' ' for x in salutation_words_df.salutation_words.tolist()]
salutation_words = set(salutation_words)
    # remove salutations
def remove_salutations(text):
    text_split = text.splitlines()
    new_text = []
    for line in text_split:
        line_temp =  re.sub('\.|\,|\!', '', line)
        line_split = line_temp.split()
        line_split = [' ' + x + ' ' for x in line_split]
        if any([x in salutation_words for x in line_split]):
            pass
        else:
            new_text.append(line)
    return '\n'.join(new_text)


# remove lines with these strings (generic text)
def remove_lines_with_generic_text(text):
    if text != None:
        
        temp_list = []
        for line in text.splitlines():
            if line.upper().startswith(("CHEERS", "DISCLAIMER", "KIND REGARDS", "THANKS & ", "CUSTOMER SUPPORT ENGINEER",
                                                "BR",  "OFFICE HOURS:", "WORK HOURS", "TEAM LEAD:", "MANAGER:", "BEST",
                                                "WITH BEST REGARDS",
                                                "MY BEST",  "MY BEST TO YOU", "ALL BEST", "ALL THE BEST", "BEST REGARDS",
                                                "BESTS", "REGARDS", "REGARDS,", "REGARDS.", "THANKS AND", "RGDS", "WARM REGARDS", 
                                                "WARMEST",
                                                "WARMLY", "THANKS,",  "MANY THANKS", "THX,", "SINCERELY,", "CHEERS!", "KIND",
                                                "CIAO", "YOURS TRULY", "VERY TRULY YOURS", "SENT FROM", ">",
                                                'TO:', 'CC:', 'PHONE', 'EMAIL','CISCO HIGH TOUCH TECHNICAL SUPPORT',
                                                'CISCO WORLDWIDE CONTACT', 'SENT:',' " <', "FROM:", "SUBJECT:","KINDLY", 
                                                'DATE', 'DIRECT MANAGER', 'OFFICE HOURS', 'CCIE', 'TO UPDATE YOUR CASE')) \
                                                | line.strip(string.punctuation).strip().upper().endswith(('.PNG', '.JPG', '.TXT', '.DAT', '.JPEG','.JPG','.XLS',
                                                                       '.XLSX','.DOC','.DOCX','.PDF')):
                        pass

            else:
                temp_list.append(line)

        final_text = '\n'.join(temp_list)
        return final_text

    
# add lemm text update
xls = pd.ExcelFile(input_files_path + 'removal_entities.xlsx')
removal_entities = pd.read_excel(xls, 'removal_entities')
# removal_entities['removal_entities'] = removal_entities['removal_entities'].apply(lemmatize_text)
removal_entities = removal_entities.drop_duplicates(subset = ['removal_entities']).reset_index(drop = True)
removal_entities = removal_entities[removal_entities['source'] == "Removal Entities"]['removal_entities'].tolist()

startswith = pd.read_excel(xls, 'startswith')
# startswith['key'] = startswith['key'].apply(lemmatize_text)
startswith = startswith.drop_duplicates(subset = ['key']).reset_index(drop = True)
key_token = pd.read_excel(xls, 'key_token')
in_order_phrases = pd.read_excel(xls, 'in_order_phrases')

pattern_list = ['(.*)' + i + '(.*?)' for i in removal_entities]
pattern_removal_entities_contains = '|'.join(pattern_list)

pattern_list = ['^' + i + '(.*?)' for i in startswith['key'].tolist()]
pattern_startswith = '|'.join(pattern_list)

removal_entities_group = key_token.groupby('key')['token'].apply(list).reset_index(name='token')
pattern_list = ['^' + i +'\s?' +  '(.*?)' +'(' + '|'.join(j) +')+' + '.*' for i,j in zip(removal_entities_group['key'].tolist(),removal_entities_group ['token'].tolist())]
pattern_removal_entities = '|'.join(pattern_list)

in_order_phrases_group = in_order_phrases.groupby('key1')['key2'].apply(list).reset_index(name='key2')                      
pattern_list = [i + '.*?' +'(' + '|'.join(j) +')' + '.*' for i,j in zip(in_order_phrases_group['key1'].tolist(), in_order_phrases_group['key2'].tolist())]
pattern_in_order_phrases = '|'.join(pattern_list)


def generic_check(sent):
    
#     lmtzr = WordNetLemmatizer()
#     sent = ' '.join([lmtzr.lemmatize(j) for j in sent.split()])      

    final_sent = sent
    # if sentence starts contains this phrase, remove the sentence
#     pattern_list = ['(.*)' + i + '(.*?)' for i in removal_entities]
#     pattern = '|'.join(pattern_list)

    if re.search(pattern_removal_entities_contains, sent):
        final_sent = ''

    elif final_sent!='':
        # if sentence starts with this phrase, remove the sentence
#         pattern_list = ['^' + i + '(.*?)' for i in startswith['key'].tolist()]
#         pattern = '|'.join(pattern_list)
        if re.search(pattern_startswith, sent):
            final_sent = ''

        elif final_sent != '':
             # if this phrase startswith key and contains token in sentence, remove the sentence
#             removal_entities_group = key_token.groupby('key')['token'].apply(list).reset_index(name='token')
#             pattern_list = ['^' + i +'\s?' +  '(.*?)' +'(' + '|'.join(j) +')+' + '.*' for i,j in zip(removal_entities_group['key'].tolist(),removal_entities_group ['token'].tolist())]
#             pattern = '|'.join(pattern_list)

            if re.search(pattern_removal_entities, sent):
                final_sent = ''

            elif final_sent!='': 
                # if sentence has these phrases in the given order, remove the sentence
#                 in_order_phrases_group = in_order_phrases.groupby('key1')['key2'].apply(list).reset_index(name='key2')                      
#                 pattern_list = [i + '.*?' +'(' + '|'.join(j) +')' + '.*' for i,j in zip(in_order_phrases_group['key1'].tolist(), in_order_phrases_group['key2'].tolist())]
#                 pattern = '|'.join(pattern_list)
                if re.search(pattern_in_order_phrases, sent):
                    final_sent = ''

                else:
                    final_sent = sent            

    return final_sent 

def clean_summary(text):

        # same case
        text = ' '.join(j.lower() for j in text.split()) 
        # non ascii
        text = ''.join([i for i in text if ord(i)<128])
        # remove hyperlinks
        text = ' '.join([re.sub(r'(http|https)?:\/\/(\w|\.|\/|\?|\=|\&|\%)*\b', ' ', i, flags=re.MULTILINE) 
                          for i in text.split()]) 

        #Remove Folder Paths Eg: /san1/EFR/sp_r53x_5_3_4/workspace/platforms/viking/fabric/drivers/arb/lib/lightning/src/lit_regio.c:565
        text = re.sub(r'\B/\w+\/{1}[\d\w-]+(\.[\d\w-]+)*(?:(?:\/[^\s/]*))*', ' ', text)

        # extra spaces 
        text = re.sub('\s+' ,' ', text)

        #Remove emails
        text = re.sub('\S*@\S*\s?',' ',text)

        # extra spaces 
        text = re.sub('\s+' ,' ', text)

        #Remove multiple instance of the repeating characters -=/_ Ex: Replace observation==== with observation
        regex  = re.compile('(\=\=+)|(\/\/+)|(\_\_+)|(\-\-+)') 
        text = re.sub(regex, "", text)

        #Remove repeating words fan fan tray = fan tray
        text = re.sub(r'\b(\w+)( \1\b)+', r'\1', text)

        # don't ": do not
        text = ' '.join([contractions_std(i) for i in text.split()])    

        # remove extra spaces
        text = re.sub('\s+', ' ', text).strip()

        #remove single occurence of alphabets sandwiched between spaces
        text = ' ' + text + ' '
        text = re.sub(' [A-z0-9] ', ' ', text)

        # remove extra spaces
        text = re.sub('\s+', ' ', text).strip()

        # remove . and ,
        text = re.sub('\.|\,' ,' ', text)
        #  print (text)

        text = re.sub('\s+' ,' ', text)
        #   print (text)

        # only valid alpha words 
        text = ' '.join([i for i in text.split() if i.isalpha() == True])

        # spell check 
        spell_corrected = []
        for i in text.split():
            if i in list(spell_correction_df.keys()):
                spell_corrected.append(spell_correction_df[i])
            else:
                spell_corrected.append(i)

        text = ' '.join(spell_corrected)

        # if all words are stop words remove line
        if all(j in stopwords_list for j in text.split())==False:
            text = remove_stopwords(text) # remove stopwords
            text = re.sub('\s+', ' ', text).strip()
        else:
            text = ''   
        
    
        # should have 2 or more words
        if len(text.split()) > 1 :
            text = {'summary_clean' : text}
        else:
            text = {'summary_clean' : ''}

        return text

def remove_sent_with_generic_text(text):
    
    if text != None:
        line_list = []
        for line in text.splitlines():
            sent_list = []
            for sent in sent_tokenize(line):
                sent_temp = sent.strip()
                sent_temp = re.sub('[^A-Za-z\s]+', ' ', sent_temp).strip() # remove non alpha characters
                
                if generic_check(sent_temp)=='':
                    pass
                else:
                    sent_list.append(sent) 
                    
            line_mod = ' '.join(sent_list)
            line_list.append(line_mod)
        final_text = '\n'.join([i for i in line_list if i!=''])
        return final_text


df_markers = pd.read_csv(input_files_path + 'stop_markers.csv', encoding = 'iso-8859-2')
markers_list = set(list(df_markers['marker']))
pat_list = ['(.*)' + i + '(.*?)' for i in markers_list]
markers_pattern = '|'.join(pat_list)

def remove_markers(text):
        if text != None:
            line_list = []
            for line in text.splitlines():
                sent_list = []
                for sent in sent_tokenize(line):
                    sent_temp = sent.strip()
                    sent_temp = re.sub('[^A-Za-z\s]+', ' ', sent_temp).strip() # remove non alpha characters
                    if re.search(markers_pattern, sent):
                        final_sent = ''
                    else:
                        sent_list.append(sent)

                line_mod = ' '.join(sent_list)
                line_list.append(line_mod)
#                 print (line_mod)
            final_text = '\n'.join([i for i in line_list if i!=''])
            return final_text


def clean_logs(text):
    if text != None:

        ## remove paras with lines > 10
        # retain only those paras having len < 10
        m = [] # list to append paras  
        for i in text.split('\n\n'):
            if len(i.splitlines())>5: # if its a para having more than 5 lines
                for j in i.splitlines():
                    if len([k for k in j.split() if k.isalpha()])>5:
                        m.append(j) # append the line if it has 6 or more valid words
                    else:
                        pass
            else:
                m.append(i) # if the para has 5 or less lines then append the entire para

        text = '\n'.join(m)

#         print (text)
#         print ('-----------')

        # remove lines having ':' and less than 5 words
        temp_list = []
        for i in text.splitlines():
            if (":" in i) and (len([j for j in i.split() if j.isalpha() == True])<5):
                pass
            else:
                temp_list.append(i)        
        text = '\n'.join(temp_list) 

#         print (text)
#         print ('-----------')

        m = []
        for i in text.splitlines():
            # atleast 2 sent and each sent has atleast 4 words valid
            if len(sent_tokenize(i))>1: # if the line contains more than 2 or more sentences
            # no punc check required
            # print (sent_tokenize(i))
                c = 0
                for j in sent_tokenize(i):
                    if len([k for k in j.split() if re.sub('\.|\,','',k).isalpha()==True])>3: # if each sent has 4 or more valid words
                        c = c+1
                if c>0:
                    m.append(i)
                text = '\n'.join(m)

            else:
                if  get_len_punc(i)<=5:
                    m.append(i)
                elif ((get_len_punc(i)>5) & (len([j for j in i.split() if re.sub('\.|\,','',j).isalpha()])>=10)):
                    m.append(i)
                else:
                    m.append(' ')
        text = '\n'.join(m)
        return text

    
def clean_question(text):
    if text!=None:

        final_text = []
        for i in text.splitlines():
            if '?' in i:
                # in a line, if sentence doesn't end with "?" then return sentence
                i = ' '.join([list_element   for list_element in  sent_tokenize(i) if list_element.endswith('?')==False])
                final_text.append(i)
            else:
                final_text.append(i)  
        text = '\n'.join(final_text)
    #     print(text)
        return text  


 # remove person names 
names_set = pickle.load(open(input_files_path+"names_set.p", 'rb'))
def remove_names(text):
    
    if text!=None:
        return '\n'.join([' '.join([j for j in i.split() if j not in names_set]) for i in text.splitlines()])

## remove leading - trailing functions
def strip_punctuations(text):
    clean_text = []
    for i in text.splitlines():
        i = i.strip(string.punctuation)
        clean_text.append(i)
    cleaned_text = '\n'.join(clean_text)    
    return cleaned_text

    # extract first n sentences
def first_n_sent(text,n):
    lines = [i for i in text.splitlines() if i!=""] # remove blank lines
    if len(lines)>0:
        lines = lines[:n]
        sent = [sent_tokenize(i) for i in lines]
        sent_list = [i for j in sent for i in j]
        return sent_list
  
    # remove first lines 
def remove_first_lines(text):
        lines = [i for i in text.splitlines() if i!=""] # remove blank lines
        if len(lines)>0:
            lines = lines[1:]
            text = '\n'.join(lines)
            return text          

# remove non ascii
def remove_non_ascii(text):
    text = text.lower()
    text = re.sub(r'[^\x00-\x7F]+',' ', text)
    text = ''.join([i for i in text if orrd(i)<128])
    text = re.sub('\s+',' ', text).strip()
    return text


def lemmatize_text(text):
    if text!=None:

        lmtzr = WordNetLemmatizer()
        lemm_text = '\n'.join([ ' '.join([lmtzr.lemmatize(j) for j in i.split()]) for i in text.splitlines()])
        return lemm_text


# sender of email
def extract_sender(text):
    text = str(text)
    pattern = 'From: (.*?)\\nTo'
    match = re.search(pattern, text)

    if match:
        return match.group(1)
    else:
        return ''
  
    # receiver of email
def extract_receiver(text):
    text = str(text)
    pattern = 'To: (.*?)\\n' #To: attach@cisco.com\nCc:
    match = re.search(pattern, text)
    l = []
    if match:
        match_mod = re.findall('<(.*?)\>',match.group())
        if len(match_mod)>0:
            return match_mod
        else:
            l.append(match.group(1))
            return str(l)
    else:
        l.append('')
        return str(l)
    
# extrat list elements in receivers' list
def extract_list_elt(text):
    text = ast.literal_eval(text)[0]
    return text
 
    # sender name
def extract_sender_name(text):
    text = str(text)
    pattern = 'From:\s?(.*)@.*\..*?'
    match = re.search(pattern, text)
    if match:
        return match.group(1)
    else:
        return ''

    # receiver email domain
def extract_receiver_domain(text):
    text = str(text)
    pattern = '.*@(.*)\..*?'
    match = re.search(pattern, text)
    if match:
        return match.group(1)
    else:
        return '' 
 
    # sender email domain
def extract_sender_domain(text):
    text = str(text)
    pattern = '.*@(.*)\..*?'
    match = re.search(pattern, text)
    if match:
        return match.group(1)
    else:
        return ''

    # email subject
def extract_email_subject(text):
    text = str(text)
    pattern = 'Subject: (.*?)\\n'
    match = re.search(pattern, text)
    if match:
        return match.group(1)
    else:
        return ''

    # email text
    
def email_start_detector(text):
    raw_text = text.replace('\n', 'NEWLINE')
    if raw_text.startswith('Note Title :'):
        pattern = 'Note Title \:.*?DetailsNEWLINE(.*)'
        match = re.search(pattern, raw_text)
        if match:
            text = match.group(1)
    else:
        text = raw_text
        
    pattern = 'From:(.*?)From:'
    match = re.search(pattern, text)
    if match:
        match_text = match.group(1)
        pattern = 'Subject:.*?NEWLINE(.*)'
        match = re.search(pattern, match_text)
        if match:
            match_text = match.group(1)
            match_text = match_text.replace('NEWLINE', '\n')
            return match_text
        else: 
            return ''
    else:
        pattern = 'Subject:.*?NEWLINE(.*)'
        match = re.search(pattern, text)
        if match:
            match_text = match.group(1)
            match_text = match_text.replace('NEWLINE', '\n')
            return match_text
        else:
            return ''

# retain these punctuations : , . ? ' "
punc = "\\".join(list('!#$%&\*+-/;<=>@[\\]^_`{|}~'))
def get_len_punc(text):
    text = str(text)
    length = len(re.findall('[' + punc + ']', text))
    return length

def remove_extra_spaces(text):
    if text != None:
        return '\n'.join([re.sub('\s+', ' ', i).strip() for i in text.splitlines()])

# drop duplicate sentences 
def drop_dup_sent(text):
    if text != None:
        x = text.splitlines()
        return '\n'.join(sorted(set(x), key = x.index))

def clean_single_letter(text):
    text = text.replace('\n', 'NEWLINE ')
    text = ' ' + text + ' '
    text = re.sub(' [A-z0-9] ', ' ', text)
    text = re.sub('\s+', ' ', text).strip()
    text = text.replace('NEWLINE ','\n')
    return text
    
def first_n_lines(text,n):
    lines = [i for i in text.splitlines() if i!=""] # remove blank lines
    if len(lines)>0:
        lines = lines[:n]
        return lines

closure_phrases_list = pd.read_csv(input_files_path+'closure_phrases_list.csv')
def remove_closure_lines(text):   
    # add in config.ini
  
    clean_text_list = []
    for i in text.splitlines():
#         print (i)
#         print ('---------------------------------')
        i_mod_list = []
        for j in sent_tokenize(i):
#             print (j)
#             print ('---------------------------------')
        
            if (any(k in j.lower() for k in closure_phrases_list) == True) & (len(j.split())<8):
                pass
            else:
                i_mod_list.append(j)
        i_mod = ' '.join(i_mod_list)
        clean_text_list.append(i_mod.strip())     
    clean_text = '\n'.join(clean_text_list)
    return clean_text

# key : values of tags clubbed together
resolution_tags = pd.read_csv(input_files_path+'resolution_tags.csv')
# resolution_tags.head()  # add in config 
std_tags = resolution_tags.groupby("key")["values"].agg(list).to_dict()
# extract list of tags 

def get_resolution_tags(text): 
    res_summ_tag_pat = ''
    for i in resolution_tags['values']:
        res_summ_tag_pat = res_summ_tag_pat + 'NEWLINE'+ i +'|'
    res_summ_tag_pat = res_summ_tag_pat[:-1]

    text = text.replace('\n', 'NEWLINE')
    text = 'NEWLINE'+text
    if len([i[7:] for i in re.findall(res_summ_tag_pat, text,re.I)])>0:
        return [i[7:] for i in re.findall(res_summ_tag_pat, text,re.I)]
    else:
        return ''
    
# print ("iterating over all rows and extracting text corr to all the tags...")
def extract_tags_data(key,text):

    key_level_ext_text = []
    for i in std_tags[key]:
        text = text.replace('\n', 'NEWLINE')
        text = 'NEWLINE'+text
        pattern = 'NEWLINE' + str(i) + r'\s?\:?\s*(.*?)(:?NEWLINE[A-z\s]+\s?:\s*)'  + '|' + 'NEWLINE' +  str(i) + r'\s?\:?\s*(.*?)(:?NEWLINE[A-z\s]+\s?:\s*)?$'
        found_pattern = re.findall(pattern, text, re.I)
        if len(found_pattern)==0: 
            extracted_text=''
        else:
            temp_list = list(found_pattern[0])
            l = [i for i in temp_list if len(i)!=0 and (temp_list.index(i)==0 or temp_list.index(i)==2)]
            if len(l)==0:
                extracted_text = ''
            else:
                extracted_text = max(l, key=len)

        if len(extracted_text)!=0:
            key_level_ext_text.append(extracted_text)
        else:
            key_level_ext_text.append('')
            
    return ' '.join([j for j in key_level_ext_text if j!='']).replace('NEWLINE','\n')



# standardize text
def std_text(text):
    
    # same case
    text = text.lower()
    # non ascii
    text = '\n'.join([''.join([j for j in i if ord(j)<128]) for i in text.splitlines()]) 
    # don't ": do not
    text = '\n'.join([contractions_std(i) for i in text.splitlines()])
    #remove brackets
    text = '\n'.join([re.sub('[(){}\[\]]', ' ', i) for i in text.splitlines()])
    # remove hyperlinks
    text = '\n'.join([re.sub(r'(http|https)?:\/\/(\w|\.|\/|\?|\=|\&|\%)*\b', ' ', i, flags=re.MULTILINE) 
                      for i in text.splitlines()]) 
    #Remove Folder Paths Eg: /san1/EFR/sp_r53x_5_3_4/workspace/platforms/viking/fabric/drivers/arb/lib/lightning/src/lit_regio.c:565
    text = '\n'.join([re.sub(r'\B/\w+\/{1}[\d\w-]+(\.[\d\w-]+)*(?:(?:\/[^\s/]*))*', ' ', i, flags=re.MULTILINE) 
                      for i in text.splitlines()]) 
    #Remove multiple instance of the repeating characters -=/_ Ex: Replace observation==== with observation
    regex  = re.compile('(\=\=+)|(\/\/+)|(\_\_+)|(\-\-+)') 
    text = '\n'.join([re.sub(regex, '', i) for i in text.splitlines()]) 
    #Remove repeating words fan fan tray = fan tray
    text = '\n'.join([re.sub(r'\b(\w+)( \1\b)+', r'\1', i) for i in text.splitlines()]) 
    # remove extra spaces
    text = '\n'.join([re.sub('\s+', ' ', i).strip() for i in text.splitlines()])
    return text


    # fasttext spell check dict on all SRS (developed on pd and summary) 
spell_correction_df = np.load(input_files_path + "inverted_index_spel_check_summ_pd_custsym.npy", allow_pickle=True).item()
# print (len(spell_correction_df.items()))
# regex to remove all time zone abreviations
time_zones = pd.read_excel(input_files_path+'time_zone_abs.xlsx')['time_zones'].to_list()
pat_tz_abs = ' '+' | '.join(time_zones)+' '


def basic_cleaning(text):
    if text != None:
        
        text = clean_single_letter(text)

        # remove all strings except alpha
        text = '\n'.join([' '.join([j for j in i.split() if j.isalpha()]) for i in text.splitlines()])

        #Remove emails
        text = re.sub('\S*@\S*\s?',' ',text)

        #Remove Phone Numbers
        text = re.sub('\s[0-9]{1,3}\s[0-9]{1,3}\s[0-9]{1,4}', ' ', text) #eg. (408 325 9873)
        text = re.sub('\s[0-9]{1,3}\s[0-9]{1,3}\s[0-9]{1,3}\s[0-9]{1,4}', ' ', text) #eg. (1 408 325 9873)    

        #Remove IP addresses
        text = re.sub(r"\s\d{2,3}\.\d{2,3}\.\d{2,3}\.\d{2,3}", ' ', text)
        text = re.sub(r"\s\d{2,3}\.\d{2,3}\.\d{2,3}", ' ', text)
        text = re.sub(r"\s\d{2,3}\.\d{2,3}\.\d{2,4}", ' ', text)
        text = re.sub(r"\s\d{2,3}\.\d{2,3}", ' ', text)

        #Remove time formats Ex:  05:09:09
        text = re.sub(r'\s\d{1,2}:\d{1,2}:\d{1,2}', ' ', text) 

        #Remove date formats Ex:  05/09 ,   05/09/2014 05-09-2014 05-Jan-2014
        text = re.sub(r'\s\d{1,4}/\d{1,4}/\d{1,4}', ' ', text)
        text = re.sub(r'\s\d{1,4}-\d{1,4}-\d{1,4}', ' ', text)
        text = re.sub(r'\s\d{1,2}/\d{1,2}', ' ', text)
        #text = re.sub(r'\d{1,4}-\w{1,4}-\d{1,4}', ' ', text)

        #Remove time formats Ex:  05:09
        text = re.sub(r'\s\d{1,2}:\d{1,2}', ' ', text)  

        #Remove date formats Ex:  05-09
        text = re.sub(r'\s\d{1,2}-\d{1,2}', ' ', text)

        text =  re.sub(r'\b[A-z]{3} [A-z]{3} \d{1,2} \d{2,4}','', text)  # Wed Dec 31 1969, Mon Apr 18 2016
        text =  re.sub(r'\b\d{1,2}:\d{1,2}:[0-9.]{1,6}', '', text) #00:03:06.753
        text =  re.sub(r'\b\d{1,2}-[A-z]{3,9}-\d{1,4}','',text ) # 05-Jan-2014
        text =  re.sub(r'\b[A-z]{3} \d{1,2} \d{2,4}','', text)  # May 16 2016

        re.sub(pat_tz_abs,' ', text)

        # spell check 
        text_temp_list = []
        for i in text.splitlines():
            spell_corrected = []
            for j in i.split():
                if j in list(spell_correction_df.keys()):
                    spell_corrected.append(spell_correction_df[j])
                else:
                    spell_corrected.append(j)
            text_temp = ' '.join(spell_corrected)
            text_temp_list.append(text_temp)

        text = '\n'.join(text_temp_list)
        
        # line should have 4 or more words 
        text = '\n'.join([i for i in text.splitlines() if len(i.split())>3])

        return text

def scrape_bugs(text):
    text = str(text)
    text = text.replace('\n', 'NEWLINE')
    bugs_list = []
    BUG_PATTERN = r'(csc\w{2,2}\d{5,5})'
    bugs_detected = re.findall(BUG_PATTERN, text.lower())
    bugs_list.extend(bugs_detected)
    if len(bugs_list)==0:
        return ''
    else:
        return bugs_list
    
def scrape_signatures(text):
    text = str(text)
    text = text.replace('\n', 'NEWLINE')
    sig_list = []
    SIG_PATTERN = r' (%[\_\-A-z0-9\/\\]+)'
    signatures_detected = re.findall(SIG_PATTERN, text.lower())
    sig_list.extend(signatures_detected)
    sig_list = [sig for sig in sig_list if len(sig) > 5]
    if len(sig_list)==0:
        return ""
    else:
        return sig_list

def scrape_sr_no(text): # check
    text = str(text)
    current_sr_no = text.split("SPLITSRBYTHIS")[0]
    text = text.split("SPLITSRBYTHIS")[1]
    text = text.replace('\n', 'NEWLINE')
    sr_no_list = []
    sr_pattern = r'6[\d]{8}'
    sr_detected = re.findall(sr_pattern, text)
    
    sr_no_list.extend([sr for sr in sr_detected if sr != str(current_sr_no)])
    
    if len(sr_no_list)==0:
        return ''
    else:
        return list(set(sr_no_list))

def extract_pid(text):
    text = str(text)
    text = ' '+text+ ' '
    pat = r'[A-Z]{1}(?=[0-9\-])[A-Z0-9\-]{9,17}'
    pid_list = re.findall(pat,text)
    if len(pid_list)>0:
        return pid_list
    else:
        return ''
             
def clean_salutation_end_phrases(text):
    cleaned_text = std_text(text)
    cleaned_text = remove_salutations(cleaned_text) # update for spell check generic data
    cleaned_text = email_end_detector(cleaned_text)
    cleaned_text = {'cleaned_v1' : cleaned_text}
    return cleaned_text

def clean_email(text):
    
    cleaned_text = clean_logs(text)
#     cleaned_text = clean_question(cleaned_text)
#     cleaned_text = remove_sent_with_generic_text(cleaned_text)
#     cleaned_text = remove_lines_with_generic_text(cleaned_text)
#     cleaned_text = remove_generic_sent(cleaned_text)
#     cleaned_text = remove_stopwords(cleaned_text)
    cleaned_text = basic_cleaning(cleaned_text)
    cleaned_text = strip_punctuations(cleaned_text)
#     cleaned_text = remove_names(cleaned_text)
    cleaned_text = remove_extra_spaces(cleaned_text)
    cleaned_text = drop_dup_sent(cleaned_text)
    cleaned_text = {'cleaned_email' : cleaned_text}
    return cleaned_text

def clean_prob_desc(text):
    text = std_text(text)
#     if "original message" in text:
#         text_temp = text.split('original message')[1]
#         text_temp = clean_salutation_end_phrases(text_temp)
#         text_temp = clean_email(text_temp)
#         cleaned_text = {'problem_desc_clean' : text_temp}
#         return cleaned_text

#     else:
    cleaned_text = clean_logs(text)
#     cleaned_text = clean_question(cleaned_text) 
#     cleaned_text = remove_lines_with_generic_text(cleaned_text)
#     cleaned_text = remove_sent_with_generic_text(cleaned_text)
    cleaned_text = remove_markers(cleaned_text) 
    #cleaned_text = remove_generic_sent(cleaned_text)
    # cleaned_text = remove_stopwords(cleaned_text)
    cleaned_text = basic_cleaning(cleaned_text)
    cleaned_text = strip_punctuations(cleaned_text)
    #cleaned_text = remove_names(cleaned_text)
    cleaned_text = remove_extra_spaces(cleaned_text)
    cleaned_text = drop_dup_sent(cleaned_text)
    cleaned_text = {'problem_desc_clean' : cleaned_text}
    return cleaned_text    
        
def clean_cust_symp(text):
    text = std_text(text)
#     if "Original Message" in text:
#         text_temp = text.split('Original Message')[1]
#         text_temp = clean_salutation_end_phrases(text_temp)
#         text_temp = clean_email(text_temp)
#         cleaned_text = {'cust_symp_clean' : text_temp}
#         return cleaned_text

    cleaned_text = clean_logs(text)
#     cleaned_text = clean_question(cleaned_text) 
#     cleaned_text = remove_lines_with_generic_text(cleaned_text)
#     cleaned_text = remove_sent_with_generic_text(cleaned_text)
    cleaned_text = remove_markers(cleaned_text) 
    #cleaned_text = remove_generic_sent(cleaned_text)
    # cleaned_text = remove_stopwords(cleaned_text)
    cleaned_text = basic_cleaning(cleaned_text)
    cleaned_text = strip_punctuations(cleaned_text)
    #cleaned_text = remove_names(cleaned_text)
    cleaned_text = remove_extra_spaces(cleaned_text)
    cleaned_text = drop_dup_sent(cleaned_text)
    cleaned_text = {'cust_symp_clean' : cleaned_text}
    return cleaned_text
    
def clean_resolution(text):
    cleaned_text = std_text(text)
    cleaned_text = clean_logs(text)
#     cleaned_text = clean_question(cleaned_text)
#     cleaned_text = remove_lines_with_generic_text(cleaned_text)
    cleaned_text = basic_cleaning(cleaned_text)
    cleaned_text = strip_punctuations(cleaned_text)
    cleaned_text = remove_closure_lines(cleaned_text)
    cleaned_text = remove_extra_spaces(cleaned_text)
    cleaned_text = drop_dup_sent(cleaned_text)

    cleaned_text = {'resolution_clean' : cleaned_text}
    return cleaned_text
    
def data_clean_pp(df,col,cleaning_function):

    processes_count = multiprocessing.cpu_count()
    print(processes_count)
    print("into parallel proc")

    final_df = pd.DataFrame()

    pool = Pool(processes = processes_count)
    temp_list = pool.map(cleaning_function,list(df[col]))
    interim_data = pd.DataFrame.from_dict(list(filter(None, temp_list)))
    interim_data['incident_number'] = list(df.incident_number)
    interim_data[col] = list(df[col])
    final_df = pd.concat([final_df, interim_data])
    print('Finished cleaning')
    return final_df

def email_start_detector(text):
    raw_text = text.replace('\n', 'NEWLINE')
    if raw_text.startswith('Note Title :'):
        pattern = 'Note Title \:.*?DetailsNEWLINE(.*)'
        match = re.search(pattern, raw_text)
        if match:
            text = match.group(1)
    else:
        text = raw_text
        
    pattern = 'From:(.*?)From:'
    match = re.search(pattern, text)
    if match:
        match_text = match.group(1)
        pattern = 'Subject:.*?NEWLINE(.*)'
        match = re.search(pattern, match_text)
        if match:
            match_text = match.group(1)
            match_text = match_text.replace('NEWLINE', '\n')
            return match_text
        else: 
            return ''
    else:
        pattern = 'Subject:.*?NEWLINE(.*)'
        match = re.search(pattern, text)
        if match:
            match_text = match.group(1)
            match_text = match_text.replace('NEWLINE', '\n')
            return match_text
        else:
            return ''

def w2v_tokenized_text(text):
    tokenized_sentences = [sent_tokenize(i) for i in text.splitlines() if len(sent_tokenize(i))>0]
    return tokenized_sentences

def w2v_input_text(tokenized_list):
    final_train_input = []
    for i in tokenized_list:
        final_train_input.append(i.split())
    return final_train_input
print ('Preprocessing Functions Imported!')